package br.com.beauty.manager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import br.com.beauty.dao.ServiceDAO;
import br.com.beauty.dao.StoreDAO;
import br.com.beauty.model.Category;
import br.com.beauty.model.Service;
import br.com.beauty.model.Statistic;
import br.com.beauty.model.Store;

public class StoreController {

	private static StoreController instance;
	private static StoreDAO storeDao;
	
	public static StoreController getInstance() {
		if (instance == null)
			instance = new StoreController();
		return instance;
	}
	
	public StoreController() {
		this.storeDao = StoreDAO.getInstance();
	}
	
	public boolean editStore (Store storeToEdit) {
		boolean sucess = false;
		if (storeToEdit.getId() != -1)
			sucess = storeDao.edit(storeToEdit);
		LoggerController.getInstance().insertLog("A loja do id "+storeToEdit.getId()+" foi editada, sucesso: "+sucess);
		return sucess;
	}
	
	public boolean insertStore(Store storeToInsert) {
		boolean sucess = storeDao.insert(storeToInsert); 
		LoggerController.getInstance().insertLog("A loja do id "+storeToInsert.getId()+" foi i, sucesso: "+sucess);
		return sucess;
	}
	
	public boolean deleteStore(int id) {
		boolean sucess = storeDao.delete(id); 
		LoggerController.getInstance().insertLog("A loja do id "+id+" foi i, sucesso: "+sucess);
		return sucess;
	}
	
	public Store getStore(int id) {
		return storeDao.getStore(id);
	}
	
	public ArrayList<Store> getListOfStores(){
		return storeDao.getListOfStores();
	}
	
	public ArrayList<Category> getListOfCategorys(){
		return storeDao.getListOfCategorys();
	}
	
	public ArrayList<Store> getListOfStores(float coordX, float coordY){
		
		ArrayList<Store> listOfStores = null;

		float range = PropertiesController.getPropertieFloat("raioDasLojas"); 
		
		try {
			
			//Calcula o raio por cordenada (0.009 = 1km)
			float rangeStores = (float) (range * 0.009);
			
			float minX = coordX-rangeStores, 
				  minY = coordY-rangeStores, 
				  maxX = coordX+rangeStores, 
				  maxY = coordY+rangeStores;
			
			listOfStores = storeDao.getListOfStores(minX, minY, maxX, maxY);
			
			if (listOfStores != null) {
				//Ordena o array de acordo com mais proximos
				for (Store store : listOfStores) {
					store.setDistanceFromDestiny(coordX, coordY);
				}
				
				Collections.sort(listOfStores, Comparator.comparing(Store::getDistanceFromDestiny));
			}
			
		}catch(NumberFormatException e) {
			e.printStackTrace();
		}
		
		return listOfStores;
	}
	
	public ArrayList<Store> getListOfStoresByTitle(float coordX, float coordY, String titleText){
		
		ArrayList<Store> listOfStores = null;
			
		listOfStores = storeDao.getListOfStores(titleText);
		
		if (listOfStores != null) {
			//Ordena o array de acordo com mais proximos
			for (Store store : listOfStores) {
				store.setDistanceFromDestiny(coordX, coordY);
			}
			
			Collections.sort(listOfStores, Comparator.comparing(Store::getDistanceFromDestiny));
		}	
			
		return listOfStores;
	}
	
}
