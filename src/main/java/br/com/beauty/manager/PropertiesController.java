package br.com.beauty.manager;

import java.io.FileReader;
import java.util.HashMap;

import org.apache.maven.model.Model;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;

import br.com.beauty.dao.PropertiesDAO;
import br.com.beauty.model.Propertie;
import commons.Logger;

public class PropertiesController {

	private static HashMap<String, Propertie> listOfProperties = init();
	
	public static HashMap<String, Propertie> init() {
		
		 HashMap<String, Propertie> listOfProperties = PropertiesDAO.getInstance().getProperties();
		
		return listOfProperties;
	}
	
	public static String getPropertie(String key) {
		String value = null;
		
		if (listOfProperties.containsKey(key)) {
			Propertie propertie = listOfProperties.get(key);
			value = propertie.getValue();
		}
		
		return value;
	}
	
	public static int getPropertieInt(String key) {
		
		String keyResponse = getPropertie(key);
		int intToReturn = 0;
		
		if (keyResponse != null) {
			try {
				intToReturn = Integer.parseInt(keyResponse);
			}catch(NumberFormatException e) {
				Logger.printErrorLogStack(e);
			}
		}
		
		return intToReturn;
	}
	
	public static float getPropertieFloat(String key) {
			
			String keyResponse = getPropertie(key);
			float floatToReturn = 0;
			
			if (keyResponse != null) {
				try {
					floatToReturn = Float.parseFloat(keyResponse);
				}catch(NumberFormatException e) {
					Logger.printErrorLogStack(e);
				}
			}
			
			return floatToReturn;
		}
	
	public static void refresh() {
		listOfProperties = PropertiesDAO.getInstance().getProperties();
	}
	
}
