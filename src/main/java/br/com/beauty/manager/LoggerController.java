package br.com.beauty.manager;

import java.util.ArrayList;

import br.com.beauty.dao.LoggerDAO;
import br.com.beauty.model.Log;

public class LoggerController {

	private static LoggerController instance;
	private static LoggerDAO contactDao;
	
	public static LoggerController getInstance() {
		if (instance == null)
			instance = new LoggerController();
		return instance;
	}
	
	public LoggerController() {
		contactDao = new LoggerDAO();
	}
	
	public void insertLog(String log) {
		try {
		  new Thread() {
	            @Override
	            public void run() {
	                contactDao.insertLog(log);
	            }
	        }.start();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void insertLog(String log, String ip) {
		try {
		  new Thread() {
	            @Override
	            public void run() {
	                contactDao.insertLog(log);
	            }
	        }.start();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public ArrayList<Log> getLogs(){
		return contactDao.getLogs();
	}
	
}
