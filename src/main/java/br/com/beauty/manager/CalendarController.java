package br.com.beauty.manager;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.TimeZone;

import br.com.beauty.dao.CalendarDAO;
import br.com.beauty.dao.StoreDAO;
import br.com.beauty.model.ScheduledService;
import commons.StringUtils;

public class CalendarController {

	private static CalendarController instance;
	private static CalendarDAO calendarDao;
	
	public static CalendarController getInstance() {
		if (instance == null)
			instance = new CalendarController();
		return instance;
	}
	
	public CalendarController() {
		calendarDao = new CalendarDAO();
	}
	
	public Calendar getToday() {
		TimeZone tz = TimeZone.getTimeZone("America/Sao_Paulo");
		TimeZone.setDefault(tz);
		Calendar today = Calendar.getInstance(tz);
		return today;
	}
	
	public String getCalendarStr() {
		Calendar today = getToday();
		return StringUtils.completeNumber(today.get(Calendar.DATE))+"/"+
		StringUtils.completeNumber((today.get(Calendar.MONTH)+1))+"/"+today.get(Calendar.YEAR)+
				", "+StringUtils.completeNumber(today.get(Calendar.HOUR_OF_DAY))+
				":"+StringUtils.completeNumber(today.get(Calendar.MINUTE));
	}
	
	public boolean ScheduledValid(long dateTime, int serviceId) throws Exception{
		boolean result = false;
		try {
			result = calendarDao.isDateValid(dateTime, serviceId);
		}catch(Exception e) {
			throw e;
		}
		return result;
	}
	
	public boolean markService(ScheduledService scheduledService) {
		LoggerController.getInstance().insertLog("O usuario "+scheduledService.getIdUser()+" marcou um novo serviço, id: "+scheduledService.getIdService());
		return calendarDao.insertScheduledService(scheduledService);
	}
	
	public HashMap<Long, Integer> getUsedDates(int serviceId){
		return calendarDao.getUsedDates(serviceId);
	}
	
	public ArrayList<ScheduledService> getSheduledServicesUser(int userId){
		ArrayList<ScheduledService> list = calendarDao.getUsedDatesFromUser(userId);
		return setExecuted(list);
	}
	
	public ArrayList<ScheduledService> getSheduledServicesStore(int storeId){
		ArrayList<ScheduledService> list = calendarDao.getUsedDatesFromStore(storeId);
		return setExecuted(list);
	}
	
	public ScheduledService getSheduledServiceStore(int sheduledId){
		return calendarDao.getScheduledService(sheduledId);
	}
	
	public ArrayList<ScheduledService> setExecuted(ArrayList<ScheduledService> list){
		
		if (list != null) {
			Calendar today = getToday();
			Timestamp current = new Timestamp(today.getTime().getTime());
			
			for (int index = 0; index < list.size(); index++) {
				if (current.after(list.get(index).getTimeStamp())) {
					list.get(index).setExecuted(true);
				}
			}
		}
		
		return list;
	}
	
	public boolean updateServiceStatus(int id, int status) {
		return calendarDao.updateScheduledServiceStatus(id, status);
	}
	
	public boolean updateScoreService(int id, int value) {
		return calendarDao.updateScoreService(id, value);
	}
	
}
