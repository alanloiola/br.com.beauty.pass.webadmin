package br.com.beauty.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import br.com.beauty.factory.ConnectionFactory;
import br.com.beauty.model.Category;
import br.com.beauty.model.Log;

public class LoggerDAO  extends ConnectionFactory {


	private static LoggerDAO instance;

	/**
	 * Metodo responsovel por criar uma instancia da classe.
	 */
	public static LoggerDAO getInstance() {
		if (instance == null)
			instance = new LoggerDAO();
		return instance;
	}
	
	public void insertLog(String message) {
		
		PreparedStatement pstmt = null;
		Connection conexao = criarConexao();

			if (conexao != null) {
			try {
				pstmt = conexao
						.prepareStatement("insert into LOGGER(title,dataEnvio)"
								+ "values(?, now())");
				pstmt.setString(1, message);
				pstmt.execute();
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
			
	}
	
	public void insertLog(String message, String ip) {
		
		PreparedStatement pstmt = null;
		Connection conexao = criarConexao();

			if (conexao != null) {
			try {
				pstmt = conexao
						.prepareStatement("insert into LOGGER(title,ip,dataEnvio)"
								+ "values(?,?, now())");
				pstmt.setString(1, message);
				pstmt.setString(2, ip);
				pstmt.execute();
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
			
	}
	
	public ArrayList<Log> getLogs(){
		
		ArrayList<Log> logs = null;
	
		Connection conexao = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		conexao = criarConexao();
		
		if (conexao != null) {
			try {
				pstmt = conexao
						.prepareStatement("SELECT * FROM LOGGER ORDER BY dataEnvio");
				rs = pstmt.executeQuery();
				
				logs = new ArrayList<>();
	
				while (rs.next()) {
					Log newLog = new Log();
					newLog.setMessage(rs.getString("title"));
					newLog.setDate(rs.getString("dataEnvio"));
					logs.add(newLog);
				}
	
			} catch (Exception e) {
				System.out.println("Erro ao listar os logs" + e);
				e.printStackTrace();
			} finally {
				fecharConexao(conexao, pstmt, rs);
			}
		}
		return logs;
	}
	
}
