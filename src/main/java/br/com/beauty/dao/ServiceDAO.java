package br.com.beauty.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.mysql.jdbc.Statement;

import br.com.beauty.factory.ConnectionFactory;
import br.com.beauty.model.*;

public class ServiceDAO  extends ConnectionFactory {


	private static ServiceDAO instance;

	/**
	 * Metodo responsovel por criar uma instancia da classe.
	 */
	public static ServiceDAO getInstance() {
		if (instance == null)
			instance = new ServiceDAO();
		return instance;
	}
		
	/**
	 * Insere uma loja
	 * @param newStore
	 * @return
	 */
	public boolean insertServices(Service serviceToAdd, int idStore) {

		boolean allRight = true;
		PreparedStatement pstmt = null;
		Connection conexao = criarConexao();

			if (conexao != null) {
			try {
				
				pstmt = conexao
						.prepareStatement("insert into servico_loja(titulo,descricao,ordem,"
								+ "id_loja,ativo,destaque,tipo_servico,opcoes_multiplica,segmento,"
								+ "limite,diasSemana,horaInicio,horaFim,duration,createDate)"
								+ "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?, now())", Statement.RETURN_GENERATED_KEYS);
				pstmt.setString(1, serviceToAdd.getTitle());
				pstmt.setString(2, serviceToAdd.getDescription());
				pstmt.setInt(3, serviceToAdd.getPosition());
				pstmt.setInt(4, idStore);
				pstmt.setBoolean(5, serviceToAdd.isValid());
				pstmt.setBoolean(6, serviceToAdd.isHightlight());
				pstmt.setInt(7, serviceToAdd.getTypeService().getId());
				pstmt.setBoolean(8, serviceToAdd.isMultiOption());
				if (serviceToAdd.getSegment() != null) {
					pstmt.setInt(9, serviceToAdd.getSegment().getId());
				}else {
					pstmt.setNull(9, Types.INTEGER);
				}
				pstmt.setInt(10, serviceToAdd.getVacanciesByTime());
				pstmt.setString(11, serviceToAdd.getDaysOfWeek().toJSONString());
				pstmt.setTime(12, serviceToAdd.getTimeStartService());
				pstmt.setTime(13, serviceToAdd.getTimeEndService());
				pstmt.setTime(14, serviceToAdd.getDuration());
				
				
				int execute = pstmt.executeUpdate();
				
				if (serviceToAdd.getServiceOption() != null && 
						serviceToAdd.getServiceOption().size() > 0) {
					 
					ResultSet resultSet = pstmt.getGeneratedKeys();
			        int newId = -1;
					if (resultSet.next()) {
						newId = resultSet.getInt(1);
			        
						allRight = insertOptionsService(serviceToAdd.getServiceOption(), newId, conexao);
					}
					
				}
				if (execute == -1)
					allRight = false;
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				allRight = false;
				e.printStackTrace();
	
			}
		}
			
		return allRight;
	}
	
	public boolean insertOptionsService(ArrayList<ServiceOption> listOfOptions, int idService) {
		Connection conexao = criarConexao();
		return insertOptionsService(listOfOptions, idService, conexao);
	}	
	
	public boolean insertOptionsService(ArrayList<ServiceOption> listOfOptions, int idService
			,Connection conexao) {
		
		boolean allRight = true;
		PreparedStatement pstmt = null;
		
			if (conexao != null) {
			try {
				for (ServiceOption serviceOption : listOfOptions) {
					pstmt = conexao
							.prepareStatement("insert into opcao_servico(id_servico,titulo,descricao,"
									+ "valor,ordem,valido)"
									+ "values(?,?,?,?,?,?)");
					pstmt.setInt(1, idService);
					pstmt.setString(2, serviceOption.getTitle());
					pstmt.setString(3, serviceOption.getDescription());
					pstmt.setDouble(4, serviceOption.getValor());
					pstmt.setInt(5, serviceOption.getOrdem());
					pstmt.setBoolean(6, serviceOption.isValid());
					boolean execute = pstmt.execute();
					
				}
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				allRight = false;
				e.printStackTrace();
	
			}
		}
			
		return allRight;
	}
	
	public int updateServicesOptions(ArrayList<ServiceOption> listOfServicesOptions) {

		int rowsModify = -1;
		PreparedStatement pstmt = null;
		Connection conexao = criarConexao();

			if (conexao != null) {
			try {
				
				rowsModify = 0;
				for (ServiceOption service : listOfServicesOptions) {
					pstmt = conexao
							.prepareStatement("update opcao_servico set "
									+ "titulo = ?, descricao = ?, valor = ?, ordem = ?, valido = ? where id = ?");
					pstmt.setString(1, service.getTitle());
					pstmt.setString(2, service.getDescription());
					pstmt.setDouble(3, service.getValor());
					pstmt.setInt(4, service.getOrdem());
					pstmt.setBoolean(5, service.isValid());
					pstmt.setInt(6, service.getId());
					
					int rowModify = pstmt.executeUpdate();
					
					if (rowModify > 0)
						rowsModify++;
				}
	
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				rowsModify = -1;
				e.printStackTrace();
	
			}finally {
				fecharConexao(conexao, pstmt, null);
			}
		}
			
		return rowsModify;
	}
	
	/**
	 * Insere uma loja
	 * @param newStore
	 * @return
	 */
	public int updateServicesShort(ArrayList<Service> listOfServices, int idStore) {

		int rowsModify = -1;
		PreparedStatement pstmt = null;
		Connection conexao = criarConexao();

			if (conexao != null) {
			try {
				
				rowsModify = 0;
				for (Service service : listOfServices) {
					pstmt = conexao
							.prepareStatement("update servico_loja set "
									+ "ordem = ?, ativo = ?, destaque = ? where id = ? and id_loja = ?");
					pstmt.setInt(1, service.getPosition());
					pstmt.setBoolean(2, service.isValid());
					pstmt.setBoolean(3, service.isHightlight());
					pstmt.setInt(4, service.getId());
					pstmt.setInt(5, idStore);
					
					int rowModify = pstmt.executeUpdate();
					
					if (rowModify > 0)
						rowsModify++;
				}
	
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				rowsModify = -1;
				e.printStackTrace();
	
			}finally {
				fecharConexao(conexao, pstmt, null);
			}
		}
			
		return rowsModify;
	}
	
	public int updateService(Service service) {

		int rowsModify = -1;
		PreparedStatement pstmt = null;
		Connection conexao = criarConexao();

			if (conexao != null) {
			try {
				
				rowsModify = 0;
				
				pstmt = conexao
						.prepareStatement("update servico_loja set titulo = ?, descricao = ?, "
								+ "ordem = ?, ativo = ?, destaque = ?, tipo_servico = ?, opcoes_multiplica = ?, "
								+ "segmento = ?, limite = ?, diasSemana = ?, horaInicio = ?, horaFim = ?, duration = ? "
								+ "where id = ?");
				pstmt.setString(1, service.getTitle());
				pstmt.setString(2, service.getDescription());
				pstmt.setInt(3, service.getPosition());
				pstmt.setBoolean(4, service.isValid());
				pstmt.setBoolean(5, service.isHightlight());
				pstmt.setInt(6, service.getTypeService().getId());
				pstmt.setBoolean(7, service.isMultiOption());
				if (service.getSegment() != null) {
					pstmt.setInt(8, service.getSegment().getId());
				}else {
					pstmt.setNull(8, Types.INTEGER);
				}
				pstmt.setInt(9, service.getVacanciesByTime());
				pstmt.setString(10, service.getDaysOfWeek().toJSONString());
				pstmt.setTime(11, service.getTimeStartService());
				pstmt.setTime(12, service.getTimeEndService());
				pstmt.setTime(13, service.getDuration());
				pstmt.setInt(14, service.getId());
				
				int rowModify = pstmt.executeUpdate();
				
				if (rowModify > 0)
					rowsModify++;
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				rowsModify = -1;
				e.printStackTrace();
	
			}finally {
				fecharConexao(conexao, pstmt, null);
			}
		}
			
		return rowsModify;
	}
	
	public ArrayList<Service> getServices(int storeId, boolean options) {

		ArrayList<Service> listOfServices = null;
		
		Connection conexao = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		conexao = criarConexao();
		
		if (conexao != null) {
			try {
				pstmt = conexao
						.prepareStatement("SELECT servico_loja.*, segmento.title as 'titleSegment', tipo.title as 'titleTypeService' "
								+ "FROM servico_loja LEFT OUTER JOIN"
								+ " segmento_servico as segmento ON servico_loja.segmento = segmento.id "
								+ "LEFT OUTER JOIN"
								+ " tipo_servico as tipo ON servico_loja.tipo_servico = tipo.id "
								+ "WHERE servico_loja.id_loja = ? ORDER BY servico_loja.ordem");
				pstmt.setInt(1, storeId);
				rs = pstmt.executeQuery();
				
				listOfServices = new ArrayList<>();
	
				while (rs.next()) {
					Service service = new Service();
					service.setId(rs.getInt("id"));
					service.setTitle(rs.getString("titulo"));
					service.setDescription(rs.getString("descricao"));
					service.setPosition(rs.getInt("ordem"));
					service.setHightlight(rs.getBoolean("destaque"));
					service.setMultiOption(rs.getBoolean("opcoes_multiplica"));
					service.setTypeService(new Category(rs.getInt("tipo_servico"),rs.getString("titleTypeService")));
					service.setValid(rs.getBoolean("ativo"));
					service.setDuration(rs.getTime("duration"));
					service.setDaysOfWeek(rs.getString("diasSemana"));
					service.setTimeStartService(rs.getTime("horaInicio"));
					service.setTimeEndService(rs.getTime("horaFim"));
					service.setVacanciesByTime(rs.getInt("limite"));
					service.setCreateDate(rs.getDate("createDate"));
					
					Object segmentObj = rs.getObject("segmento");
					if (segmentObj != null)
						service.setSegment(new Category(rs.getInt("segmento"),rs.getString("titleSegment")));
					listOfServices.add(service);
				}
				
				if (options)
					listOfServices = getOptionsOfService(listOfServices, conexao);
	
			} catch (Exception e) {
				System.out.println("Erro ao obter os serviços da loja: " + storeId+ " - "+e);
				e.printStackTrace();
			} finally {
				fecharConexao(conexao, pstmt, rs);
			}
		}
		return listOfServices;
	}
	
	public Service getService(int serviceId) {
		
		Connection conexao = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		conexao = criarConexao();
		Service service = null;
		
		if (conexao != null) {
			try {
				pstmt = conexao
						.prepareStatement("SELECT servico_loja.*, segmento.title as 'titleSegment', tipo.title as 'titleTypeService' "
								+ "FROM servico_loja LEFT OUTER JOIN"
								+ " segmento_servico as segmento ON servico_loja.segmento = segmento.id "
								+ "LEFT OUTER JOIN"
								+ " tipo_servico as tipo ON servico_loja.tipo_servico = tipo.id "
								+ "WHERE servico_loja.id = ?");
				pstmt.setInt(1, serviceId);
				rs = pstmt.executeQuery();
				
				if (rs.next()) {
					service = new Service();
					service.setId(rs.getInt("id"));
					service.setTitle(rs.getString("titulo"));
					service.setDescription(rs.getString("descricao"));
					service.setPosition(rs.getInt("ordem"));
					service.setHightlight(rs.getBoolean("destaque"));
					service.setMultiOption(rs.getBoolean("opcoes_multiplica"));
					service.setTypeService(new Category(rs.getInt("tipo_servico"),rs.getString("titleTypeService")));
					service.setValid(rs.getBoolean("ativo"));
					service.setDuration(rs.getTime("duration"));
					service.setDaysOfWeek(rs.getString("diasSemana"));
					service.setTimeStartService(rs.getTime("horaInicio"));
					service.setTimeEndService(rs.getTime("horaFim"));
					service.setVacanciesByTime(rs.getInt("limite"));
					service.setIdStore(rs.getInt("id_loja"));
					service.setCreateDate(rs.getDate("createDate"));
					
					Object segmentObj = rs.getObject("segmento");
					if (segmentObj != null)
						service.setSegment(new Category(rs.getInt("segmento"),rs.getString("titleSegment")));
					
				}
				
				service = getOptionsOfService(service, conexao);
				
			} catch (Exception e) {
				System.out.println("Erro ao obter o serviço: " + serviceId+ " - "+e);
				e.printStackTrace();
			} finally {
				fecharConexao(conexao, pstmt, rs);
			}
		}
		return service;
	}
	
	public ArrayList<Service> getOptionsOfService(ArrayList<Service> services, Connection conexao){

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String whereOptions = getWhereByServices(services);
		if (conexao != null && !whereOptions.isEmpty()) {
			
			HashMap<Integer, ArrayList<ServiceOption>> listHash = new HashMap<>();
			
			try {
				pstmt = conexao
						.prepareStatement("SELECT * FROM opcao_servico"
								+ " WHERE id_servico in "+whereOptions+" ORDER BY ordem");
				rs = pstmt.executeQuery();
	
				while (rs.next()) {
					ServiceOption service = new ServiceOption();
					int idServico = rs.getInt("id_servico");
					service.setId(rs.getInt("id"));
					service.setTitle(rs.getString("titulo"));
					service.setDescription(rs.getString("descricao"));
					service.setValid(rs.getBoolean("valido"));
					service.setValor(rs.getDouble("valor"));
					service.setOrdem(rs.getInt("ordem"));
					service.setServiceId(idServico);
					
					if (!listHash.containsKey(idServico)) {
						listHash.put(idServico, new ArrayList<ServiceOption>());
					}
					
					listHash.get(idServico).add(service);
				}
				
				for (Service service : services) {
					service.setServiceOption(listHash.get(service.getId()));
				}
	
			} catch (Exception e) {
				System.out.println("Erro ao obter as opções dos serviços - "+e);
				e.printStackTrace();
			}
		}
		return services;
		
	}

	
	public Service getOptionsOfService(Service service, Connection conexao){

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		if (conexao != null && service != null) {
			
			ArrayList<ServiceOption> list = new ArrayList<>();
			
			try {
				pstmt = conexao
						.prepareStatement("SELECT * FROM opcao_servico WHERE id_servico = "+service.getId()+" ORDER BY ordem");
				rs = pstmt.executeQuery();
	
				while (rs.next()) {
					ServiceOption serviceOp = new ServiceOption();
					int idServico = rs.getInt("id_servico");
					serviceOp.setId(rs.getInt("id"));
					serviceOp.setTitle(rs.getString("titulo"));
					serviceOp.setDescription(rs.getString("descricao"));
					serviceOp.setValid(rs.getBoolean("valido"));
					serviceOp.setValor(rs.getDouble("valor"));
					serviceOp.setOrdem(rs.getInt("ordem"));
					serviceOp.setServiceId(idServico);
					list.add(serviceOp);
				}
				
				service.setServiceOption(list);
	
			} catch (Exception e) {
				System.out.println("Erro ao obter as opções dos serviços - "+e);
				e.printStackTrace();
			}
		}
		return service;
	}

	public boolean removeServiceOptions(List<String> listOfIds) {

		boolean sucess = true;
		
		Connection conexao = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		conexao = criarConexao();
		
		if (conexao != null) {
			try {
				
				for (String idToDelete : listOfIds) {
					
					pstmt = conexao
							.prepareStatement("DELETE FROM opcao_servico WHERE id = ?");
					pstmt.setInt(1, Integer.parseInt(idToDelete));
					pstmt.execute();
				
				}
				
			} catch (Exception e) {
				System.out.println("Erro ao apagar as opções de serviço - "+e);
				sucess = false;
				e.printStackTrace();
			} finally {
				fecharConexao(conexao, pstmt, rs);
			}
		}
		return sucess;
	}
	
	public boolean removeServices(ArrayList<String> listOfIds, int storeId) {

		boolean sucess = false;
		
		Connection conexao = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		conexao = criarConexao();
		
		if (conexao != null) {
			try {
				
				for (String idToDelete : listOfIds) {
					pstmt = conexao
							.prepareStatement("DELETE FROM opcao_servico WHERE id_servico = ?");
					pstmt.setInt(1, Integer.parseInt(idToDelete));
					boolean exec = pstmt.execute();
					
					pstmt = conexao
							.prepareStatement("DELETE FROM servico_loja WHERE id = ? AND id_loja = ?");
					pstmt.setInt(1, Integer.parseInt(idToDelete));
					pstmt.setInt(2, storeId);
					exec = pstmt.execute();
					
					if (!exec)
						sucess = false;
					
				}
				
			} catch (Exception e) {
				System.out.println("Erro ao apagar os serviços da loja: " + storeId+ " - "+e);
				sucess = false;
				e.printStackTrace();
			} finally {
				fecharConexao(conexao, pstmt, rs);
			}
		}
		return sucess;
	}
	
	public ArrayList<Category> getListOfCategorys(){
		
		ArrayList<Category> categorysToReturn = null;
	
		Connection conexao = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		conexao = criarConexao();
		
		if (conexao != null) {
			try {
				pstmt = conexao
						.prepareStatement("SELECT * FROM tipo_servico ORDER BY title");
				rs = pstmt.executeQuery();
				
				categorysToReturn = new ArrayList<>();
	
				while (rs.next()) {
					Category category = new Category(rs.getInt("id"), rs.getString("title"));
					
					categorysToReturn.add(category);
				}
	
			} catch (Exception e) {
				System.out.println("Erro ao listar todos as categoryas: " + e);
				e.printStackTrace();
			} finally {
				fecharConexao(conexao, pstmt, rs);
			}
		}
		return categorysToReturn;
		
	}
	
	public boolean insertSegmets(ArrayList<String> listOfSegmentsToAdd, int idLoja) {

		boolean allRight = true;
		PreparedStatement pstmt = null;
		Connection conexao = criarConexao();

			if (conexao != null) {
			try {
				
				for (String title : listOfSegmentsToAdd) {
					pstmt = conexao
							.prepareStatement("insert into segmento_servico(title,id_loja)"
									+ "values(?,?)");
					pstmt.setString(1, title);
					pstmt.setInt(2, idLoja);
					pstmt.execute();
					
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				allRight = false;
				e.printStackTrace();
	
			}
		}
			
		return allRight;
	}
	
	public ArrayList<Category> getListOfSegments(int idStore){
		
		ArrayList<Category> categorysToReturn = null;
	
		Connection conexao = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		conexao = criarConexao();
		
		if (conexao != null) {
			try {
				pstmt = conexao
						.prepareStatement("SELECT * FROM segmento_servico where id_loja = ? ORDER BY title");
				pstmt.setInt(1, idStore);
				rs = pstmt.executeQuery();
				
				categorysToReturn = new ArrayList<>();
	
				while (rs.next()) {
					Category category = new Category(rs.getInt("id"), rs.getString("title"));
					
					categorysToReturn.add(category);
				}
	
			} catch (Exception e) {
				System.out.println("Erro ao listar todos as categoryas: " + e);
				e.printStackTrace();
			} finally {
				fecharConexao(conexao, pstmt, rs);
			}
		}
		return categorysToReturn;
	}
	
	public boolean removeSegments(ArrayList<String> listOfIds) {

		boolean sucess = false;
		
		Connection conexao = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		conexao = criarConexao();
		
		if (conexao != null) {
			try {
				
				for (String idToDelete : listOfIds) {
					
					pstmt = conexao
							.prepareStatement("UPDATE servico_loja set segmento = null WHERE segmento = ?");
					pstmt.setInt(1, Integer.parseInt(idToDelete));
					boolean exec = pstmt.execute();
					
					pstmt = conexao
							.prepareStatement("DELETE FROM segmento_servico WHERE id = ?");
					pstmt.setInt(1, Integer.parseInt(idToDelete));
					exec = pstmt.execute();
					
					if (!exec)
						sucess = false;
					
				}
				
			} catch (Exception e) {
				System.out.println("Erro ao apagar os segmentos - "+e);
				sucess = false;
				e.printStackTrace();
			} finally {
				fecharConexao(conexao, pstmt, rs);
			}
		}
		return sucess;
	}
	
	private static String getWhereByServices(ArrayList<Service> listOfServices) {
		StringBuilder strWhere = new StringBuilder();
		if (listOfServices.size() > 0) {
			strWhere.append("(");
			for (int index = 0; index < listOfServices.size(); index++) {
				strWhere.append(listOfServices.get(index).getId());
				if (index < listOfServices.size()-1)
					strWhere.append(",");
			}
			strWhere.append(")");
		}
		return strWhere.toString();
	}
	
	public boolean updateSegments(ArrayList<Category> listOfSegments, int idStore) {

		boolean sucess = true;
		PreparedStatement pstmt = null;
		Connection conexao = criarConexao();

			if (conexao != null) {
			try {
				
				for (Category category : listOfSegments) {
					pstmt = conexao
							.prepareStatement("update segmento_servico set "
									+ "title = ? where id = ? and id_loja = ?");
					pstmt.setString(1, category.getName());
					pstmt.setInt(2, category.getId());
					pstmt.setInt(3, idStore);
					pstmt.executeUpdate();
					
				}
	
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				sucess = false;
				e.printStackTrace();
	
			}finally {
				fecharConexao(conexao, pstmt, null);
			}
		}
			
		return sucess;
	}
	
	
	
}
