package br.com.beauty.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import br.com.beauty.factory.ConnectionFactory;
import br.com.beauty.model.Category;
import br.com.beauty.model.User;
import br.com.beauty.model.Propertie;

public class PropertiesDAO extends ConnectionFactory {


	private static PropertiesDAO instance;

	/**
	 * Metodo responsovel por criar uma instancia da classe.
	 */
	public static PropertiesDAO getInstance() {
		if (instance == null)
			instance = new PropertiesDAO();
		return instance;
	}
	
	/**
	 * Obtem todas as propriedades
	 * @return
	 */
	public HashMap<String, Propertie> getProperties() {

		Connection conexao = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		conexao = criarConexao();
		HashMap<String, Propertie> listOfProperties = new HashMap<>();

		if (conexao != null) {
			try {
				pstmt = conexao
						.prepareStatement("SELECT * FROM properties");
			
				rs = pstmt.executeQuery();
				while (rs.next()) {
					String key = rs.getString("chave");
					String value = rs.getString("valor");
					int protection = rs.getInt("protecao");
					Propertie nova = new Propertie(key, value, protection);
					listOfProperties.put(key, nova);
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				fecharConexao(conexao, pstmt, rs);
			}
		}
		return listOfProperties;
	}
	
	public ArrayList<Category> getListOfTypes(){
			
			ArrayList<Category> typesToReturn = null;
		
			Connection conexao = null;
			PreparedStatement pstmt = null;
			ResultSet rs = null;
	
			conexao = criarConexao();
			
			if (conexao != null) {
				try {
					pstmt = conexao
							.prepareStatement("SELECT * FROM typesToReturn ORDER BY title");
				rs = pstmt.executeQuery();
				
				typesToReturn = new ArrayList<>();
	
				while (rs.next()) {
					Category category = new Category(rs.getInt("id"), rs.getString("title"));
					
					typesToReturn.add(category);
				}
	
			} catch (Exception e) {
				System.out.println("Erro ao listar todos os tipos de serviço: " + e);
				e.printStackTrace();
			} finally {
				fecharConexao(conexao, pstmt, rs);
			}
		}
		return typesToReturn;
	
	}

}
