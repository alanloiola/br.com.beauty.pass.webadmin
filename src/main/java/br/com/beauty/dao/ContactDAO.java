package br.com.beauty.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import br.com.beauty.factory.ConnectionFactory;
import br.com.beauty.model.Category;
import br.com.beauty.model.Contact;
import br.com.beauty.model.Service;
import br.com.beauty.model.ServiceOption;
import br.com.beauty.model.User;

public class ContactDAO  extends ConnectionFactory {


	private static ContactDAO instance;

	/**
	 * Metodo responsovel por criar uma instancia da classe.
	 */
	public static ContactDAO getInstance() {
		if (instance == null)
			instance = new ContactDAO();
		return instance;
	}
	
	public boolean insertContact(Contact contact) {

		boolean allRight = true;
		PreparedStatement pstmt = null;
		Connection conexao = criarConexao();

			if (conexao != null) {
			try {
				
					pstmt = conexao
							.prepareStatement("insert into contato(idServicoAgendado,texto,dataEnvio)"
									+ "values(?,?, NOW())");
					pstmt.setInt(1, contact.getIdServiceExecuted());
					pstmt.setString(2, contact.getMessage());
					
					boolean execute = pstmt.execute();
					
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				allRight = false;
				e.printStackTrace();
	
			}
		}
			
		return allRight;
	}
	
	public ArrayList<Contact> getListOfContact(){
		
		ArrayList<Contact> listOfContacts = null;
		
		Connection conexao = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		conexao = criarConexao();
		
		if (conexao != null) {
			try {
				pstmt = conexao
						.prepareStatement("SELECT * FROM contato " + 
								"INNER JOIN servico_agendado ON contato.idServicoAgendado = servico_agendado.id " + 
								"INNER JOIN cliente ON cliente.id = servico_agendado.idUser ");
				rs = pstmt.executeQuery();
				
				listOfContacts = new ArrayList<>();
	
				while (rs.next()) {
					
					Contact newContact = new Contact();
					newContact.setId(rs.getInt("id"));
					newContact.setIdServiceExecuted(rs.getInt("idServicoAgendado"));
					newContact.setMessage(rs.getString("texto"));
					newContact.setDateSend(rs.getDate("dataEnvio"));
				
					User client = new User();
					client.setId(rs.getInt("cliente.id"));
					client.setNome(rs.getString("cliente.nome"));
					newContact.setUser(client);
					
					listOfContacts.add(newContact);
					
				}
				
			} catch (Exception e) {
				System.out.println("Erro ao obter os contatos: "+e);
				e.printStackTrace();
			} finally {
				fecharConexao(conexao, pstmt, rs);
			}
		}
		return listOfContacts;
		
	}
	

}
