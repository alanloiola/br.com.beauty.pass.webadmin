package br.com.beauty.dao;

import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import br.com.beauty.factory.ConnectionFactory;
import br.com.beauty.model.Category;
import br.com.beauty.model.Service;
import br.com.beauty.model.Store;

public class StoreDAO extends ConnectionFactory {


	private static StoreDAO instance;
	private String selectStore = " SELECT estabelecimento.*, cat.*, quantidade_avaliacoes.avaliacoes FROM estabelecimento\r\n" + 
			"        INNER JOIN categoria_estabelecimento as cat ON estabelecimento.categoria = cat.id \r\n" + 
			"        LEFT JOIN (SELECT idLoja, COUNT(avaliacao) AS \"avaliacoes\" FROM servico_agendado WHERE avaliacao > -1 GROUP BY idLoja) \r\n" + 
			"        AS quantidade_avaliacoes on quantidade_avaliacoes.idLoja = estabelecimento.id";

	/**
	 * Metodo responsovel por criar uma instancia da classe.
	 */
	public static StoreDAO getInstance() {
		if (instance == null)
			instance = new StoreDAO();
		return instance;
	}
	
	/**
	 * Insere uma loja
	 * @param newStore
	 * @return
	 */
	public boolean insert(Store newStore) {

		boolean isGravado = false;
		PreparedStatement pstmt = null;
		Connection conexao = criarConexao();

			if (conexao != null) {
			try {
				pstmt = conexao
						.prepareStatement("insert into estabelecimento(nome,introducao,descricao,email,telefone,senha,cnpj,endereco,cep,coordX,coordY,imageid,categoria,precoLvl,dataCriacao)"
								+ "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?, now())");
				pstmt.setString(1, newStore.getTitle());
				pstmt.setString(2, newStore.getIntroduction());
				pstmt.setString(3, newStore.getDescription());
				pstmt.setString(4, newStore.getEmail());
				pstmt.setString(5, newStore.getTelefone());
				pstmt.setString(6, newStore.getPassword());
				pstmt.setString(7, newStore.getCnpj());
				pstmt.setString(8, newStore.getAdress());
				pstmt.setString(9, newStore.getCep());
				pstmt.setFloat(10, newStore.getCoordX());
				pstmt.setFloat(11, newStore.getCoordY());
				pstmt.setInt(12, newStore.getIdImage());
				pstmt.setInt(13, newStore.getCategory().getId());
				pstmt.setInt(14, newStore.getPriceLvl());
				
				boolean execute = pstmt.execute();
				isGravado = true;
	
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				isGravado = false;
				e.printStackTrace();
	
			}
		}
			
		return isGravado;
	}
	
	/**
	 * Insere uma loja
	 * @param newStore
	 * @return
	 */
	public boolean edit(Store newStore) {

		boolean isGravado = false;
		PreparedStatement pstmt = null;
		Connection conexao = criarConexao();

			if (conexao != null) {
			try {
				pstmt = conexao
						.prepareStatement("update estabelecimento set nome = ?, introducao = ?, descricao = ?, email = ?, telefone = ?, senha = ?, cnpj = ?,"
								+ " endereco = ?, cep = ?, coordX = ?, coordY = ?, categoria = ?, precoLvl = ? where id = ?");
				pstmt.setString(1, newStore.getTitle());
				pstmt.setString(2, newStore.getIntroduction());
				pstmt.setString(3, newStore.getDescription());
				pstmt.setString(4, newStore.getEmail());
				pstmt.setString(5, newStore.getTelefone());
				pstmt.setString(6, newStore.getPassword());
				pstmt.setString(7, newStore.getCnpj());
				pstmt.setString(8, newStore.getAdress());
				pstmt.setString(9, newStore.getCep());
				pstmt.setFloat(10, newStore.getCoordX());
				pstmt.setFloat(11, newStore.getCoordY());
				pstmt.setInt(12, newStore.getCategory().getId());
				pstmt.setInt(13, newStore.getPriceLvl());
				pstmt.setInt(14, newStore.getId());
				
				boolean execute = pstmt.execute();
				isGravado = true;
	
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				isGravado = false;
				e.printStackTrace();
	
			}
		}
			
		return isGravado;
	}
	
	public boolean delete(int id) {

		boolean executed = false;
		PreparedStatement pstmt = null;
		Connection conexao = criarConexao();

			if (conexao != null) {
			try {

				pstmt = conexao
						.prepareStatement("delete from servico_agendado where idLoja = ?");
				pstmt.setInt(1, id);
				executed = pstmt.execute();
				pstmt = conexao
						.prepareStatement("delete from opcao_servico where id_servico in "
								+ "(select id from servico_loja where id_loja = ?)");
				pstmt.setInt(1, id);
				executed = pstmt.execute();

				pstmt = conexao
						.prepareStatement("delete from segmento_servico where id_loja = ?");
				pstmt.setInt(1, id);
				executed = pstmt.execute();
				
				pstmt = conexao
						.prepareStatement("delete from servico_loja where id_loja = ?");
				pstmt.setInt(1, id);
				executed = pstmt.execute();
				
				pstmt = conexao
						.prepareStatement("delete from estabelecimento where id = ?");
				pstmt.setInt(1, id);
				executed = pstmt.execute();
				
				executed = true;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				executed = false;
				e.printStackTrace();
	
			}
		}
			
		return executed;
	}
	
	
	
	
	
	/**
	 * Obtem uma lista de lojas 
	 * 
	 */
	public ArrayList<Store> getListOfStores(){
		
		ArrayList<Store> listOfStores = null;
		
		Connection conexao = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		conexao = criarConexao();
		
		if (conexao != null) {
			try {
				pstmt = conexao
						.prepareStatement(selectStore);
				rs = pstmt.executeQuery();
				
				listOfStores = new ArrayList<>();
	
				while (rs.next()) {
					Store store = getStore(rs);
					store.setCategory(rs.getInt("categoria"), rs.getString("nome_categoria"));
					
					listOfStores.add(store);
				}
	
			} catch (Exception e) {
				System.out.println("Erro ao listar todos as lojas: " + e);
				e.printStackTrace();
			} finally {
				fecharConexao(conexao, pstmt, rs);
			}
		}
		return listOfStores;
	}
	
	/**
	 * Obtem uma lista de lojas 
	 * 
	 */
	public ArrayList<Store> getListOfStores(float minX, float minY, float maxX, float maxY){

		ArrayList<Store> listOfStores = null;
		
		Connection conexao = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		conexao = criarConexao();

		if (conexao != null) {
			try {
				pstmt = conexao
						.prepareStatement(selectStore
								+ " where coordX >= ? AND coordX <= ? AND coordY >= ? AND coordY <= ? ORDER BY dataCriacao");
				pstmt.setFloat(1, minX);
				pstmt.setFloat(2, maxX);
				pstmt.setFloat(3, minY);
				pstmt.setFloat(4, maxY);
				rs = pstmt.executeQuery();
	
				listOfStores = new ArrayList<>();
				
				while (rs.next()) {
					Store store = getStore(rs);
					listOfStores.add(store);
				}
	
			} catch (Exception e) {
				System.out.println("Erro ao listar todos as lojas: " + e);
				e.printStackTrace();
			} finally {
				fecharConexao(conexao, pstmt, rs);
			}
		}
		return listOfStores;
	}


	public ArrayList<Store> getListOfStores(String searchText){

		ArrayList<Store> listOfStores = null;
		
		searchText = searchText.replaceAll(" ", "|");
		ServiceDAO serviceDAO = new ServiceDAO();
		
		Connection conexao = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		conexao = criarConexao();

		if (conexao != null) {
			try {
				
				HashMap<Integer, ArrayList<Service>> listOfServices = new HashMap<>();
				
				pstmt = conexao
						.prepareStatement("SELECT * FROM servico_loja where titulo REGEXP ? OR descricao REGEXP ?");
				pstmt.setString(1, searchText);
				pstmt.setString(2, searchText);
				rs = pstmt.executeQuery();
				
				while (rs.next()) {
					Service newService = new Service();
					newService.setId(rs.getInt("id"));
					newService.setIdStore(rs.getInt("id_loja"));
					newService.setTitle(rs.getString("titulo"));
					newService.setDescription(rs.getString("descricao"));
					newService.setDuration(rs.getTime("duration"));
					
					newService = serviceDAO.getOptionsOfService(newService, conexao);
					
					if (listOfServices.containsKey(newService.getIdStore())) {
						listOfServices.get(newService.getIdStore()).add(newService);
					}else {
						ArrayList<Service> newsServices = new ArrayList<>();
						newsServices.add(newService);
						listOfServices.put(newService.getIdStore(), newsServices);
					}
					
				}
				
				String whereClausule = " where nome REGEXP ? ";
				if (!listOfServices.isEmpty()) {
					whereClausule += "OR estabelecimento.id in "+listOfServices.keySet().toString().replace('[', '(').replace(']', ')');
				}
				
				pstmt = conexao
						.prepareStatement(selectStore
								+ whereClausule+" ORDER BY dataCriacao");
				pstmt.setString(1, searchText);
				rs = pstmt.executeQuery();
	
				listOfStores = new ArrayList<>();
				
				while (rs.next()) {
					Store store = getStore(rs);
					if (listOfServices.containsKey(store.getId())) {
						store.setListOfServices(listOfServices.get(store.getId()));
					}
					listOfStores.add(store);
				}
	
			} catch (Exception e) {
				System.out.println("Erro ao listar todos as lojas: " + e);
				e.printStackTrace();
			} finally {
				fecharConexao(conexao, pstmt, rs);
			}
		}
		return listOfStores;
	}

	
	
	public ArrayList<Category> getListOfCategorys(){
		
		ArrayList<Category> categorysToReturn = null;
	
		Connection conexao = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		conexao = criarConexao();
		
		if (conexao != null) {
			try {
				pstmt = conexao
						.prepareStatement("SELECT * FROM categoria_estabelecimento ORDER BY nome_categoria");
				rs = pstmt.executeQuery();
				
				categorysToReturn = new ArrayList<>();
	
				while (rs.next()) {
					Category category = new Category(rs.getInt("id"), rs.getString("nome_categoria"));
					category.setImageId(rs.getInt("imageId"));
					categorysToReturn.add(category);
				}
	
			} catch (Exception e) {
				System.out.println("Erro ao listar todos as categoryas: " + e);
				e.printStackTrace();
			} finally {
				fecharConexao(conexao, pstmt, rs);
			}
		}
		return categorysToReturn;
		
	}
	
	public Store getStore(int id) {
		
		Store storeToReturn = null;
		
		Connection conexao = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		conexao = criarConexao();
		
		if (conexao != null) {
			try {
				pstmt = conexao
						.prepareStatement(selectStore+" WHERE estabelecimento.id = ? ORDER BY dataCriacao");
				pstmt.setInt(1, id);
				rs = pstmt.executeQuery();
	
				if (rs.next()) {
					storeToReturn = getStore(rs);
				}
	
			} catch (Exception e) {
				System.out.println("Erro ao lista a loja: " + id+ " - "+e);
				e.printStackTrace();
			} finally {
				fecharConexao(conexao, pstmt, rs);
			}
		}
		return storeToReturn;
	}
	
	private Store getStore(ResultSet rs) throws SQLException {
		Store storeToReturn = new Store();
		storeToReturn.setId(rs.getInt("id"));
		storeToReturn.setTitle(rs.getString("nome"));
		storeToReturn.setIntroduction(rs.getString("introducao"));
		storeToReturn.setDescription(rs.getString("descricao"));
		storeToReturn.setEmail(rs.getString("email"));
		storeToReturn.setTelefone(rs.getString("telefone"));
		storeToReturn.setCnpj(rs.getString("cnpj"));
		storeToReturn.setAdress(rs.getString("endereco"));
		storeToReturn.setCep(rs.getString("cep"));
		storeToReturn.setComplement(rs.getString("complemento"));
		storeToReturn.setValid(rs.getBoolean("valido"));
		storeToReturn.setCoordX(rs.getFloat("coordX"));
		storeToReturn.setCoordY(rs.getFloat("coordY"));
		storeToReturn.setIdImage(rs.getInt("imageid"));
		storeToReturn.setCreateDate(rs.getDate("dataCriacao"));
		if (rs.getObject("avaliacoes") != null)
			storeToReturn.setCountAvaliable(rs.getInt("avaliacoes"));
		storeToReturn.setPriceLvl(rs.getInt("precoLvl"));
		storeToReturn.setScore(rs.getFloat("nota"));
		storeToReturn.setCategory(rs.getInt("categoria"), rs.getString("nome_categoria"), rs.getInt("cat.imageId"));
		return storeToReturn;
	}
		
}
