package br.com.beauty.exception;

public class FormatInvalidServiceScheduled extends Exception{
    private String msg;
    public FormatInvalidServiceScheduled(String msg){
      super(msg);
      this.msg = msg;
    }
    public String getMessage(){
      return msg;
    }
  }