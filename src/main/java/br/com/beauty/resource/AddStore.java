package br.com.beauty.resource;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.beauty.manager.StoreController;
import br.com.beauty.model.Category;
import br.com.beauty.model.Store;
import commons.StoreUtils;
import commons.StringUtils;

/**
 * Adiciona uma nova loja
 * @author aloiola
 *
 */
public class AddStore extends HttpServlet {
   
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");

        //Status
        String status = "Erro inesperado";
        boolean error = true;
        
        Store storeToInsert = StoreUtils.getStoreByPost(request);

        if (storeToInsert != null) {
        	StoreController controller = StoreController.getInstance();
        	error = !controller.insertStore(storeToInsert);
        }
	    
        try (PrintWriter writer = response.getWriter()) {

            writer.println("<!DOCTYPE html><html>");
            writer.println("<head>");
            writer.println("<meta charset=\"UTF-8\" />");
            writer.println("<title>Loja inserida</title>");
            writer.println("</head>");
            writer.println("<body>");
            writer.println("<a href=\"/listaDeLojas.jsp\">voltar</a>");
            
            if (error) {
                writer.println("<h1>Houve um erro</h1>");
                writer.println("<p>Houve um erro ao processar o cadastro: "+status+"</p>");
            }else {
            	writer.println("<h1>Loja inserida com sucesso!</h1>");
            }
            
            writer.println("</body>");
            writer.println("</html>");
        }
        
        
    }

}
