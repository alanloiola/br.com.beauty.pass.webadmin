package br.com.beauty.resource;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import br.com.beauty.manager.StoreController;

/**
 * Remove uma loja
 * @author aloiola
 *
 */
public class RemoveStore extends HttpServlet {
	   
		protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	        response.setContentType("text/html");
	        response.setCharacterEncoding("UTF-8");
		
	        boolean sucess = false;
	        String idStr = request.getParameter("idStore");
	        if (idStr != null) {
	        	try {
	        		int id = Integer.parseInt(idStr);
	    			StoreController controller = StoreController.getInstance();
	    			sucess = controller.deleteStore(id);
	        	}catch(Exception e) {
	        		e.printStackTrace();
	        	}
	        }
	        
	        PrintWriter out = response.getWriter();
		    response.setContentType("application/json;charset=UTF-8");
		    response.setCharacterEncoding("UTF-8");
		    JSONObject jsonResponse = new JSONObject();
		    jsonResponse.put("sucesso", sucess);
		    out.print(jsonResponse.toJSONString());
		    out.flush();
		}

}
