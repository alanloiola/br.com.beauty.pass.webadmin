package br.com.beauty.resource;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import br.com.beauty.exception.FormatInvalidServiceScheduled;
import br.com.beauty.manager.CalendarController;
import br.com.beauty.manager.PropertiesController;
import br.com.beauty.manager.ServiceController;
import br.com.beauty.manager.StoreController;
import br.com.beauty.model.ScheduledService;
import br.com.beauty.model.Store;
import br.com.beauty.model.TimeService;
import commons.ServiceUtils;
import commons.StringUtils;
import commons.WebServiceUtils;

/**
 * Controle do calendario dos serviços
 * @author aloiola
 *
 */
public class ServiceCalendar extends HttpServlet {
	
	/**
	 * Obtem os horarios vagos de acordo com um serviceId
	 */
	@Override
	 public void doGet(HttpServletRequest request, HttpServletResponse response)
	      throws IOException {

			JSONObject jsonResponse = new JSONObject();
			ServiceController controller = ServiceController.getInstance();
			CalendarController calendarController = CalendarController.getInstance();
			
			boolean sucess = true;
			String statusMessage = "sucesso";

			Object serviceId = request.getHeader("serviceId");
			
			if (serviceId != null) {
	
				//Obtem o id do serviço
				int id = Integer.parseInt(serviceId.toString());
				HashMap<Long, TimeService> listOfDate = null;
				
				//Obtem as datas disponiveis do serviço
				try {
					 listOfDate = controller.getCalendarServiceDisp(id);
				}catch (Exception e) {
					statusMessage = e.getMessage();
					sucess = false;
				}
				
				if (listOfDate != null) {
					
					JSONArray jsonCalendar = new JSONArray();
					
					Set<Long> keysSet = listOfDate.keySet();
					List<Long> sortedListKeys = new ArrayList<>(keysSet);
					Collections.sort(sortedListKeys);
					
					//Monta as datas de retorno no serviço
					for (Long dateTime : sortedListKeys) {
					
						TimeService timeService = listOfDate.get(dateTime);
						
						ArrayList<Timestamp> listOfTimesDay = timeService.getListOfTimes();
						
						JSONObject dateParameters = new JSONObject();
						dateParameters.put("date", timeService.getDateString());
						dateParameters.put("dayOfWeek", timeService.getDayOfWeek());
						dateParameters.put("completeDate", timeService.getCompleteDate());
						
						JSONArray listOfTimes = new JSONArray();
						
						//Monta os horarios disponiveis
						for (Timestamp time: listOfTimesDay) {
							JSONObject jsonTimeObj = new JSONObject();
							String timeDescription = StringUtils.completeNumber(time.getHours())
										+":"+StringUtils.completeNumber(time.getMinutes());

							jsonTimeObj.put("time", timeDescription);
							jsonTimeObj.put("timeDate", time.getTime());
							listOfTimes.add(jsonTimeObj);
						}
						
						dateParameters.put("vagancies", listOfTimes);
						//dateParameters.put("timeDate", timeService.getTime().getTime());
						
						if (!listOfTimesDay.isEmpty())
							jsonCalendar.add(dateParameters);
						
					}
					
					jsonResponse.put("calendar", jsonCalendar);
					
				}else if (sucess == true){
					sucess = false;
					statusMessage = "Erro ao obter os horarios vagos";
				}
        		jsonResponse.put("idService", id);
			}else {
				sucess = false;
				statusMessage = "Erro no parametro";
			}

			jsonResponse.put("timeServer", calendarController.getCalendarStr());
			jsonResponse.put("sucess", sucess);
			jsonResponse.put("responseTxt", statusMessage);
		    
			PrintWriter out = response.getWriter();
		    response.setContentType("application/json;charset=UTF-8");
		    response.setCharacterEncoding("UTF-8");
		    out.print(jsonResponse.toJSONString());
		    out.flush();
	  }
	
	/**
	 * Recebe os parametros para marcar um serviço
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			
		JSONObject jsonResponse = new JSONObject();

		boolean sucess = true;
		String statusMessage = "sucesso";

		String body = WebServiceUtils.extractPostRequestBody(request);
		CalendarController calendarController = CalendarController.getInstance();

		if (body != null && !body.isEmpty()) {
			
			ScheduledService serviceToMark = null;
			try {
				serviceToMark = ServiceUtils.getscheduledService(body);
			}catch(FormatInvalidServiceScheduled formatExe) {
				statusMessage = formatExe.getMessage();
				sucess = false;
			}catch ( ParseException parser) {
				statusMessage = "Erro ao formatar o JSON";
				sucess = false;
			}catch (Exception e) {
				e.printStackTrace();
			}
			
			if (serviceToMark != null) {
				
				boolean hasInBd = true;
				
				try {
					hasInBd = calendarController.ScheduledValid(serviceToMark.getDateTime(), serviceToMark.getIdService());
				}catch(Exception e) {
					sucess = false;
					statusMessage = "Erro ao verificar a disponibilidade do horario";
				}
				
				if (sucess) {
					if (hasInBd) { 
						sucess = false;
						statusMessage = "Horario indisponivel";
					}else{
						boolean inserted = false;
						
						inserted = calendarController.markService(serviceToMark);
						
						if (!inserted) {
							sucess = false;
							statusMessage = "Erro ao salvar os dados no banco de dados";
						}
						
					}
				}
				
			}else if (sucess == true){
				sucess = false;
				statusMessage = "Erro ao obter as informações do request";
			}
		}else {
			sucess = false;
			statusMessage = "Body vazio";
		}
		
		jsonResponse.put("timeServer", calendarController.getCalendarStr());
		jsonResponse.put("sucess", sucess);
		jsonResponse.put("responseTxt", statusMessage);
		
		PrintWriter out = response.getWriter();
	    response.setContentType("application/json;charset=UTF-8");
	    response.setCharacterEncoding("UTF-8");
	    out.print(jsonResponse.toJSONString());
	    out.flush();
	}

}
