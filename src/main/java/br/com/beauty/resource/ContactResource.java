package br.com.beauty.resource;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import br.com.beauty.exception.FormatInvalidServiceScheduled;
import br.com.beauty.manager.CalendarController;
import br.com.beauty.manager.ContactController;
import br.com.beauty.model.Contact;
import br.com.beauty.model.ScheduledService;
import commons.ServiceUtils;
import commons.WebServiceUtils;

/**
 * Adiciona um novo contato
 * @author aloiola
 *
 */
public class ContactResource  extends HttpServlet {
	   
		protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			JSONObject jsonResponse = new JSONObject();

			boolean sucess = true;
			String statusMessage = "sucesso";

			String body = WebServiceUtils.extractPostRequestBody(request);
			ContactController contactController = ContactController.getInstance();

			if (body != null && !body.isEmpty()) {
				
				JSONObject jsonBody = null;
				JSONParser parser = new JSONParser();
				Contact contact = new Contact();
				
				try {
					jsonBody = (JSONObject) parser.parse(body);
					
					int id = Integer.parseInt(jsonBody.get("id").toString());
					String message = jsonBody.get("message").toString();
					
					contact.setIdServiceExecuted(id);
					contact.setMessage(message);
					
				}catch(Exception e) {
					e.printStackTrace();
					contact = null;
				}
				
				if (contact != null) {
					sucess = contactController.insertContact(contact);
					if (!sucess)
						statusMessage = "Erro ao inserir os dados no banco";
				}else{
					sucess = false;
					statusMessage = "Erro ao processar os dados do body";
				}
				
			}else {
				sucess = false;
				statusMessage = "Body vazio";
			}
			
			jsonResponse.put("sucess", sucess);
			jsonResponse.put("responseTxt", statusMessage);
			
			PrintWriter out = response.getWriter();
		    response.setContentType("application/json;charset=UTF-8");
		    response.setCharacterEncoding("UTF-8");
		    out.print(jsonResponse.toJSONString());
		    out.flush();
		
		}

}
