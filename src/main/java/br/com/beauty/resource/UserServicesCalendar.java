package br.com.beauty.resource;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import br.com.beauty.manager.CalendarController;
import br.com.beauty.manager.PropertiesController;
import br.com.beauty.manager.StoreController;
import br.com.beauty.model.ScheduledService;
import br.com.beauty.model.Store;

/**
 * Obtem informações sobre o calendario de um usuario
 * @author aloiola
 *
 */
public class UserServicesCalendar extends HttpServlet {
	  
	@Override
	 public void doGet(HttpServletRequest request, HttpServletResponse response)
	      throws IOException {

			JSONObject jsonResponse = new JSONObject();
			StoreController controller = StoreController.getInstance();
			
			boolean sucess = true;
			String statusMessage = "sucesso";
			CalendarController calendarController = CalendarController.getInstance();
			 
			Object userId = request.getHeader("userId");
			
			if (userId != null && !userId.toString().isEmpty()) {
				
				int idUser = -1;
				try {
					idUser = Integer.parseInt(userId.toString());
				}catch(NumberFormatException e) {
					sucess = false;
					statusMessage = "Parametro de ID invalido";
				}
				
				if (idUser > 0) {
					
					 ArrayList<ScheduledService> listOfScheduledService = calendarController.getSheduledServicesUser(idUser);
					 
					 if (listOfScheduledService != null) {
						
						 JSONArray jsonList = new JSONArray();
						 Calendar today = calendarController.getToday();
						 
						 for (ScheduledService scheduledService : listOfScheduledService) {
							 jsonList.add(scheduledService.toJSONObject());
						 }

						 jsonResponse.put("services", jsonList);
						 jsonResponse.put("servicesSize", jsonList.size());
						 
						 
					 }else {
						sucess = false;
						statusMessage = "Erro ao obter os dados do banco de dados";
					 }
					 
					jsonResponse.put("userId", idUser);
				}
				
			}else {
				sucess = false;
				statusMessage = "Erro ao ler o parametro do id do user";
			}

			jsonResponse.put("timeServer", calendarController.getCalendarStr());
			jsonResponse.put("sucess", sucess);
			jsonResponse.put("responseTxt", statusMessage);
			
		    
			PrintWriter out = response.getWriter();
		    response.setContentType("application/json;charset=UTF-8");
		    response.setCharacterEncoding("UTF-8");
		    out.print(jsonResponse.toJSONString());
		    out.flush();
	  }
	

}
