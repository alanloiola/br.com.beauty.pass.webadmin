package br.com.beauty.resource;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import br.com.beauty.manager.UserController;
import br.com.beauty.manager.ServiceController;
import br.com.beauty.model.User;

/**
 * Obtem um novo usuario
 * @author aloiola
 *
 */
public class UserResource extends HttpServlet {
	  
	@Override
	 public void doGet(HttpServletRequest request, HttpServletResponse response)
	      throws IOException {

			JSONObject jsonResponse = new JSONObject();
			UserController controller = UserController.getInstance();

			boolean sucess = true;
			String statusMessage = "sucesso";

			
			String ipUser = request.getRemoteAddr();
			String localeUser = request.getHeader("userLocation");
			User client = controller.insertUser(ipUser, localeUser);
			
			if (client != null) {
				
			}else {
				sucess = false;
				statusMessage = "Erro ao inserir o novo usuario";
			}
			
			jsonResponse.put("newUser", client==null?null:client.toJSONObject());
			jsonResponse.put("sucess", sucess);
			jsonResponse.put("responseTxt", statusMessage);
		    
			PrintWriter out = response.getWriter();
		    response.setContentType("application/json;charset=UTF-8");
		    response.setCharacterEncoding("UTF-8");
		    out.print(jsonResponse.toJSONString());
		    out.flush();
	  }

}
