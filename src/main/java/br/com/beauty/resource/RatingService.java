package br.com.beauty.resource;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import br.com.beauty.manager.CalendarController;
import br.com.beauty.manager.ContactController;
import br.com.beauty.model.Contact;
import commons.WebServiceUtils;

/**
 * Realiza a avaliação de alguma loja
 * @author aloiola
 *
 */
public class RatingService extends HttpServlet {
	   
		protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			JSONObject jsonResponse = new JSONObject();

			boolean sucess = true;
			String statusMessage = "sucesso";

			String body = WebServiceUtils.extractPostRequestBody(request);
			CalendarController calendarController = CalendarController.getInstance();

			if (body != null && !body.isEmpty()) {
				
				JSONObject jsonBody = null;
				JSONParser parser = new JSONParser();
				
				int id = -1, value = -1;
				
				try {
					jsonBody = (JSONObject) parser.parse(body);
					
					id = Integer.parseInt(jsonBody.get("id").toString());
					value = Integer.parseInt(jsonBody.get("value").toString());
					
				}catch(Exception e) {
					e.printStackTrace();
				}
				
				if (id > -1 && value > 0 && value <= 6) {
					sucess = calendarController.updateScoreService(id, value);
					if (!sucess)
						statusMessage = "Erro ao inserir os dados no banco";
				}else{
					sucess = false;
					statusMessage = "Erro ao processar os dados do body";
				}
				
			}else {
				sucess = false;
				statusMessage = "Body vazio";
			}
			
			jsonResponse.put("sucess", sucess);
			jsonResponse.put("responseTxt", statusMessage);
			
			PrintWriter out = response.getWriter();
		    response.setContentType("application/json;charset=UTF-8");
		    response.setCharacterEncoding("UTF-8");
		    out.print(jsonResponse.toJSONString());
		    out.flush();
		
		}

}
