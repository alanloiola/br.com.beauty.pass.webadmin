package br.com.beauty.resource;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.beauty.manager.ServiceController;
import br.com.beauty.manager.StoreController;
import br.com.beauty.model.Category;
import br.com.beauty.model.ServiceOption;
import br.com.beauty.model.Store;
import commons.ServiceUtils;
import commons.StoreUtils;

/**
 * Edita/Cria os itens dos segmentos
 * @author aloiola
 *
 */
public class SegmentManager extends HttpServlet {
	   
		protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	        response.setContentType("text/html");
	        response.setCharacterEncoding("UTF-8");

	        //Status
	        String status = "Erro inesperado";
	        boolean error = true;
	        ServiceController controller = ServiceController.getInstance();
	        
	        String idStr = request.getParameter("storeId");
	        
	        if (idStr != null ) {
		        int id = Integer.parseInt(idStr);
		        
		        String strSegmentsAdd = request.getParameter("segmentsToAdd");
		        
		        if (strSegmentsAdd != null) {
			        //Adiciona os itens
			        ArrayList<String> segmentsToAdd = StoreUtils.getSegmentsToAdd(strSegmentsAdd);
		        	if (segmentsToAdd != null && !strSegmentsAdd.isEmpty())
		        		error = !controller.insertSegments(segmentsToAdd, id);
			    }
		        
		        String strSegmentsRemove = request.getParameter("segmentsToDelete");
		        if (strSegmentsRemove != null) {
		        	ArrayList<String> segmentsToDelete = StoreUtils.getIdsByJSONDelete(strSegmentsRemove);
		        	if (segmentsToDelete != null && !segmentsToDelete.isEmpty())
		        		error = !controller.removeSegments(segmentsToDelete);
		        }
		        
		        String strSegmentsEdit = request.getParameter("segmentsToEdit");
		        if (strSegmentsEdit != null && !strSegmentsEdit.replace("{}", "").trim().isEmpty()) {
		        	ArrayList<Category> segmentsToEdit = StoreUtils.getCategoryByJSONEdit(strSegmentsEdit);
		        	if (!segmentsToEdit.isEmpty()) 
		        		error = !controller.editSegments(segmentsToEdit, id);
		        }
		        
		        
	        }
	        
	        try (PrintWriter writer = response.getWriter()) {

	            writer.println("<!DOCTYPE html><html>");
	            writer.println("<head>");
	            writer.println("<meta charset=\"UTF-8\" />");
	            writer.println("<title>Segmento inserido</title>");
	            writer.println("</head>");
	            writer.println("<body>");
	            writer.println("<a href=\"/services/servicos.jsp?storeId="+idStr+"\">voltar</a>");
	            
	            if (error) {
	                writer.println("<h1>Houve um erro</h1>");
	                writer.println("<p>Houve um erro ao processar o cadastro: "+status+"</p>");
	            }else {
	            	writer.println("<h1>Segmentos alterados com sucesso!</h1>");
	            }
	            
	            writer.println("</body>");
	            writer.println("</html>");
	        }
	        
	        
	    }

}
