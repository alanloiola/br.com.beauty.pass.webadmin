package br.com.beauty.model;

import org.json.simple.JSONObject;

public class ServiceOption {
	
	private int id,
				ordem,
				serviceId;
	private String title,
			description;
	private double valor;
	private boolean valid;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setId(String id) {
		this.id = Integer.parseInt(id);
	}
	public int getOrdem() {
		return ordem;
	}
	public void setOrdem(int ordem) {
		this.ordem = ordem;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getValor() {
		return valor;
	}
	public void setValor(double valor) {
		this.valor = valor;
	}
	public boolean isValid() {
		return valid;
	}
	public void setValid(boolean valid) {
		this.valid = valid;
	}
	public int getServiceId() {
		return serviceId;
	}
	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}
	public JSONObject getJSONObject() {
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("id", id);
		jsonObj.put("title", title);
		jsonObj.put("value", valor);
		jsonObj.put("valid", valid);
		jsonObj.put("position", ordem);
		jsonObj.put("description", description);
		return jsonObj;
	}

}
