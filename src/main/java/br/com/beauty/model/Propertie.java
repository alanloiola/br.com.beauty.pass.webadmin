package br.com.beauty.model;

public class Propertie {

	private String key,
				value;
	private int protection;
	
	public Propertie(String key, String value, int protection) {
		this.key = key;
		this.value = value;
		this.protection = protection;
	}
	
	public String getKey() {
		return key;
	}
	public String getValue() {
		return value;
	}
	public int getProtection() {
		return protection;
	}
	
	
	
}
