package br.com.beauty.model;

import java.util.ArrayList;

public class Statistic {

	private ArrayList<ServiceStatistic> listOfServices;
	private Store store;
	private int qtAval,
			qtDeclined,
			qtAcept,
			qtNotConfirmed,
			qtAcess;
	private float totalValue;
	
	public ArrayList<ServiceStatistic> getListOfServices() {
		return listOfServices;
	}
	public void setListOfServices(ArrayList<ServiceStatistic> listOfServices) {
		this.listOfServices = listOfServices;
	}
	public int getQtAval() {
		return qtAval;
	}
	public void setQtAval(int qtAval) {
		this.qtAval = qtAval;
	}
	public int getQtDeclined() {
		return qtDeclined;
	}
	public void setQtDeclined(int qtDeclined) {
		this.qtDeclined = qtDeclined;
	}
	public int getQtAcept() {
		return qtAcept;
	}
	public void setQtAcept(int qtAcept) {
		this.qtAcept = qtAcept;
	}
	public int getQtNotConfirmed() {
		return qtNotConfirmed;
	}
	public void setQtNotConfirmed(int qtNotConfirmed) {
		this.qtNotConfirmed = qtNotConfirmed;
	}
	public int getQtAcess() {
		return qtAcess;
	}
	public void setQtAcess(int qtAcess) {
		this.qtAcess = qtAcess;
	}
	public Store getStore() {
		return store;
	}
	public void setStore(Store store) {
		this.store = store;
	}
	public float getTotalValue() {
		return totalValue;
	}
	public void setTotalValue(float totalValue) {
		this.totalValue = totalValue;
	}
	
}
