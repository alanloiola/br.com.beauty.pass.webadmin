package br.com.beauty.factory;

import com.google.appengine.api.utils.SystemProperty;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Classe responsavel por conter os metodos para criar e fechar o banco de dados.
 */
public class ConnectionFactory {
	
	private String SENHA_MYSQL;
	private String HOST_MYSQL;
	private String URL_MYSQL;
	private String PORTA_MYSQL;
	private String USUARIO_MYSQL;
	private String DBNAME;
	private String INSTANCECONNECTIONNAME;

	/**
	 * Metodo responsavel por criar uma conexao com o banco
	 */
	public Connection criarConexao() {

		// conexão local
		HOST_MYSQL = "34.69.122.73";
		PORTA_MYSQL = "3306";
		USUARIO_MYSQL = "app";
		SENHA_MYSQL = "beautypassword";
		DBNAME = "beautyDB";
		INSTANCECONNECTIONNAME = "beauty-pass-admin:us-central1:beautypassdb";
		
		try {
	      
			//Valida se está na nuvem ou local
		  if (SystemProperty.environment.value() == null) {
			  URL_MYSQL = "jdbc:mysql://google/"+DBNAME+"?cloudSqlInstance="+INSTANCECONNECTIONNAME+"&socketFactory=com.google.cloud.sql.mysql.SocketFactory&user="+USUARIO_MYSQL+"&password="+SENHA_MYSQL;
			//Class.forName("com.mysql.jdbc.GoogleDriver");
	      } else {
	    	URL_MYSQL = "jdbc:mysql://" + HOST_MYSQL + ":" + PORTA_MYSQL + "/beautyDB";
	      }
	      
	    } catch (Exception e) {
	      e.printStackTrace();
	      return null;
	    }
		
		Connection conexao = null;

		try {
	        Class.forName("com.mysql.jdbc.Driver");
			conexao = DriverManager.getConnection(URL_MYSQL, USUARIO_MYSQL, SENHA_MYSQL);
		} catch (Exception e) {
			System.out.println("Erro ao criar conexao com o banco: " + URL_MYSQL);
			e.printStackTrace();
		}
		return conexao;
	}

	public void fecharConexao(Connection conexao, PreparedStatement pstmt, ResultSet rs) {

		try {

			if (conexao != null) {
				conexao.close();
			}
			if (pstmt != null) {
				pstmt.close();
			}
			if (rs != null) {
				rs.close();
			}
			
		} catch (Exception e) {
			System.out.println("Erro ao fechar conexao com o banco: " + URL_MYSQL);
		}
	}
}
