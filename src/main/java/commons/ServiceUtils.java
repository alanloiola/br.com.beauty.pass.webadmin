package commons;

import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import br.com.beauty.exception.FormatInvalidServiceScheduled;
import br.com.beauty.model.ScheduledService;
import br.com.beauty.model.Service;
import br.com.beauty.model.ServiceOption;

public class ServiceUtils {

	public static Service getServiceByRequest(HttpServletRequest request) {
		
		Service newService = new Service();
		
		Object id = request.getParameter("id");
		if (id != null) {
			newService.setId(Integer.parseInt(id.toString()));
		}
		
		newService.setIdStore(Integer.parseInt(request.getParameter("storeId")));
		newService.setTitle(request.getParameter("title"));
		newService.setDescription(request.getParameter("description"));
		newService.setPosition(Integer.parseInt(request.getParameter("ordem")));
		newService.setValid(request.getParameter("ativo")==null?false:request.getParameter("ativo").equals("on"));
		newService.setHightlight(request.getParameter("destaque")==null?false:request.getParameter("destaque").equals("on"));
		newService.setTypeService(Integer.parseInt(request.getParameter("typeService")));
		newService.setMultiOption(request.getParameter("muilti")==null?false:request.getParameter("muilti").equals("on"));
		
		String segment = request.getParameter("segment");
		if (segment != null && !segment.trim().isEmpty())
			newService.setSegment(Integer.parseInt(segment));
		
		//Dias da semana
		JSONArray daysOfWeek = new JSONArray();
		for(int day = 0; day < 7; day++) {
			Object dayObj = request.getParameter("day"+day);
			if (dayObj != null) {
				if (dayObj.toString().equals("on")) {
					daysOfWeek.add(day);
				}
			}
		}
		

		newService.setDaysOfWeek(daysOfWeek);
		newService.setTimeStartService(StringUtils.getTime(request.getParameter("inicioHora")));
		newService.setTimeEndService(StringUtils.getTime(request.getParameter("fimHora")));
		newService.setDuration(StringUtils.getTime(request.getParameter("duration")));
		newService.setVacanciesByTime(Integer.parseInt(request.getParameter("limite")));
		
		return newService;
	}
	
	public static ArrayList<ServiceOption> getServiceOptions(String serviceOptionsStr){
		
		JSONParser jsonParse = new JSONParser();
		JSONArray jsonArrOptions;
		try {
			jsonArrOptions = (JSONArray) jsonParse.parse(serviceOptionsStr);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
		
		ArrayList<ServiceOption> listOfIdServices = null;
		
		if (jsonArrOptions.size() > 0) {
			listOfIdServices = new ArrayList<>();
			for (int index = 0; index < jsonArrOptions.size(); index++) {
				JSONObject jsonObj = (JSONObject)jsonArrOptions.get(index);
				if (jsonObj != null)
					listOfIdServices.add(getServiceOptionByJSON(jsonObj));
			}
		}
		return listOfIdServices;
	}
	
	public static ServiceOption getServiceOptionByJSON(JSONObject jsonObj) {
		
		ServiceOption service = new ServiceOption();

		service.setId(Integer.parseInt(jsonObj.get("id").toString()));
		service.setTitle(jsonObj.get("name").toString());
		service.setOrdem(Integer.parseInt(jsonObj.get("position").toString()));
		service.setValid(jsonObj.get("valid").toString()=="true");
		service.setValor(Double.parseDouble(jsonObj.get("valor").toString()));
		service.setDescription(jsonObj.get("description")==null?null:jsonObj.get("description").toString());
		
		return service;
	}
	
	public static ScheduledService getscheduledService(String requestBody) throws Exception,ParseException,FormatInvalidServiceScheduled {
		
		JSONParser jsonParse = new JSONParser();
		JSONObject jsonObjResquest =  null;
		
		try {
			jsonObjResquest = (JSONObject) jsonParse.parse(requestBody);
		}catch (ParseException parse) {
			throw new ParseException(0);
		}
		
		ScheduledService newService = new ScheduledService();
		String campo = "serviceId";
		
		try {
			Object serviceId = jsonObjResquest.get(campo);
			if(!validateField(serviceId)) {
				throw new FormatInvalidServiceScheduled(campo+" invalido ou vazio");
			}
			newService.setIdService(Integer.parseInt(serviceId.toString()));
			
			campo = "idUser";
			Object idUser = jsonObjResquest.get(campo);
			if(!validateField(idUser)) {
				throw new FormatInvalidServiceScheduled(campo+" invalido ou vazio");
			}
			newService.setIdUser(Integer.parseInt(idUser.toString()));
			
			campo = "dateTime";
			Object dateTime = jsonObjResquest.get(campo);
			if(!validateField(dateTime)) {
				throw new FormatInvalidServiceScheduled(campo+" invalido ou vazio");
			}
			newService.setDateTime(Long.parseLong(dateTime.toString()));
			
			campo = "value";
			Object value = jsonObjResquest.get(campo);
			if(!validateField(value)) {
				throw new FormatInvalidServiceScheduled(campo+" invalido ou vazio");
			}
			newService.setValue(Double.parseDouble(value.toString()));
			
			campo = "options";
			Object options = jsonObjResquest.get(campo);
			if(!validateField(options)) {
				throw new FormatInvalidServiceScheduled(campo+" invalido ou vazio");
			}
			newService.setOptions(options.toString());
		
		}catch(Exception e) {
			throw new FormatInvalidServiceScheduled("Erro no campo: "+campo);
		}
		return newService;
	}
	
	private static boolean validateField(Object field) {
		if (field == null || field.toString().isEmpty())
			return false;
		return true;
	}
	
}
