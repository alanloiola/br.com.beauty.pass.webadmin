package commons;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;

public class CalcUtils {

	/**
	 * Calcula a distancia entre dois pontos
	 * @return
	 */
	public static double distance(double lat1, double lat2, double lon1, double lon2) {

	    final int R = 6371; // Radius of the earth

	    Double latDistance = deg2rad(lat2 - lat1);
	    Double lonDistance = deg2rad(lon2 - lon1);
	    Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
	            + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2))
	            * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
	    Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
	    double distance = R * c * 1000; // convert to meters
	    distance = Math.pow(distance, 2);
	    return (double) Math.sqrt(distance);
	}
	
	private static double deg2rad(double deg) {
	    return (deg * Math.PI / 180.0);
	}
	
	public static double metersToKm(double meters) {
		return  meters/1000;
	}
	
	public static Long timeToMiliSeconds(Time time) {
		return Long.valueOf((time.getMinutes()+(time.getHours()*60))*60000);
	}
	
	public static Timestamp sumTime(Timestamp time1, Time time2) {
		time1.setHours(time1.getHours()+time2.getHours());
		time1.setMinutes(time1.getMinutes()+time2.getMinutes());
		return time1;
	}

	public static boolean isAfterTime(Timestamp time1, Timestamp time2) {
		boolean isAfter = false;
		
		if ((time1.getHours() == time2.getHours()
				&& time1.getMinutes() > time2.getMinutes())
				|| time1.getHours() > time2.getHours()) {
			isAfter = true;
		}
		
		return isAfter;
	}
	
	public static boolean isAfterTime(Time time1, Timestamp time2) {
		boolean isAfter = false;
		
		if ((time1.getHours() == time2.getHours()
				&& time1.getMinutes() > time2.getMinutes())
				|| time1.getHours() > time2.getHours()) {
			isAfter = true;
		}
		
		return isAfter;
	}
	
	 public static double roundNumber(double val, int qtCasas){
        BigDecimal bd = new BigDecimal(val).setScale(qtCasas, RoundingMode.HALF_EVEN);
        return bd.doubleValue();
    }
	
}
