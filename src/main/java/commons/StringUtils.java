package commons;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class StringUtils {

	private static JSONParser parser = new JSONParser();
	private static String[] months = { "Janeiro", "Fevereiro", "Março", "Abril", 
			"Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" };
	private static String[] daysOfWeek = {"Domingo","Segunda-Feira","Terça-Feira","Quarta-Feira","Quinta-Feira","Sexta-Feira","Sábado"};
	/**
	 * Criptografa uma string
	 * @param value
	 * @return
	 */
	public static String criptoString(String value) {
		
		String result = null;
		if (value != null) {
			try {
				MessageDigest message = MessageDigest.getInstance("MD5");
			 	message.update(value.getBytes(),0, value.length());
		     	result = new BigInteger(1,message.digest()).toString(16);
			}catch (Exception e) {
				Logger.printErrorLogStack("Erro ao criptografar uma string", e);
				e.printStackTrace();
			}
		}
	     
	     return result;
	}
	
	public static String completeNumber(int number) {
		if (number < 10) {
			return "0"+number;
		}
		return Integer.toString(number);
	}
	
	public static Time getTime(String time) {
		String timeSplited[] = time.split(":");
		int hour = Integer.parseInt(timeSplited[0]);
		int minutes = Integer.parseInt(timeSplited[1]);
		return (new Time(hour, minutes, 0));
	}
	
	public static JSONArray toJSONArr(String str) {
		try {
			return (JSONArray)parser.parse(str);	
		}catch(Exception e) {
			return null;
		}
	}
	
	public static JSONObject toJSONObj(String str) {
		try {
			return (JSONObject)parser.parse(str);	
		}catch(Exception e) {
			return null;
		}
	}
	
	public static String getTimeFormated(Time time) {
		return completeNumber(time.getHours())+":"+completeNumber(time.getMinutes());
	}
	
	public static String getDateFormated(Date timeDay) {
		return completeNumber(timeDay.getDate())+"/"+
					completeNumber(timeDay.getMonth()+1)+"/"+(timeDay.getYear()+1900);
	}
	
	public static String getDateFormated(Timestamp timeDay) {
		return completeNumber(timeDay.getDate())+"/"+
					completeNumber(timeDay.getMonth()+1)+"/"+(timeDay.getYear()+1900);
	}
	
	public static String getDayOfWeek(int day) {
		if (day < daysOfWeek.length && day > -1)
			return daysOfWeek[day];
		return null;
	}
	
	public static String getMonth(int month) {
		if (month < months.length && month > -1)
			return months[month];
		return null;
	}
	
	public static String getCompleteDate(Timestamp timeDay) {
		return getDayOfWeek(timeDay.getDay())+", "+
				timeDay.getDate() + " de "+getMonth(timeDay.getMonth())+" de "+(timeDay.getYear()+1900);
	}
	
	public static String getCompleteDate(Date timeDay) {
		return getDayOfWeek(timeDay.getDay())+", "+
				timeDay.getDate() + " de "+getMonth(timeDay.getMonth())+" de "+(timeDay.getYear()+1900);
	}
	
	public static String getSimplificateDate(Timestamp timestamp) {
		return completeNumber(timestamp.getDate())+"/"+
				completeNumber(timestamp.getMonth()+1)+" às "+completeNumber(timestamp.getHours())+":"
				+completeNumber(timestamp.getMinutes());
	}
	
}
