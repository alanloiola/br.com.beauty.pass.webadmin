<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<%@page import="br.com.beauty.model.*,
		br.com.beauty.manager.StoreController,
		java.util.ArrayList"%>
  <head>
  	<%@ include file="headImports.html"%>
    <title>Editar loja</title>
    <meta charset="utf-8"/>
    <script src="/vendor/jquery-3.4.1.min.js"></script>
  </head>
  <body>
  	<%
  		String idOnParameter = request.getParameter("storeId");
	  	Store store = null;	
	  	
	  	if (idOnParameter != null){
	  		
	  		int id;
	  		try{
	  			id = Integer.parseInt(idOnParameter);
	  			store = StoreController.getInstance().getStore(id);	
	  		}catch(NumberFormatException e){
	  			System.out.print("Erro ao obter os dados do id para editar a loja");
	  		}
	  	}
  		
  	%>
  <a href="/listaDeLojas.jsp">voltar</a>
    <h1>Editar loja</h1>
    <br/>
    <% if (store != null){ 
    	ArrayList<Category> listOfCategorys = StoreController.getInstance().getListOfCategorys();
    %>
    <form action="editStore" method="POST">
   	 <input type="hidden" name="id" value="<%=store.getId()%>">
       ID: <input type="number" disabled value="<%=store.getId()%>">
    	<br />
      Título da loja: <input type="text" name="title" maxlength="255" value="<%=store.getTitle()%>">
      <br />
      Introdução: <input type="text" name="introduction" required value="<%=store.getIntroduction()%>"/>
      <br />
      Descrição: <textarea type="text" name="description" value="<%=store.getDescription()%>"><%=store.getDescription()%></textarea>
      <br />
      email: <input type="email" name="email" required value="<%=store.getEmail()%>"/>
      <br />
      telefone: <input type="text" name="tel" required value="<%=store.getTelefone()%>"/>
      <br />
      senha: <input type="password" name="password" required value="<%=store.getPassword()%>"/>
      <br />
      cnpj: <input type="number" name="cnpj" required value="<%=store.getCnpj()%>"/>
      <br />
      endereco: <input type="text" name="endereco" required value="<%=store.getAdress()%>"/>
      <br />
      CEP: <input type="number" maxlength="9" name="cep" required value="<%=store.getCep()%>"/>
      <br />
      Coord X: <input type="text" maxlength="30" name="coordX" required placeholder="1.0" value="<%=store.getCoordX()%>"/>
      <br />
      Coord Y: <input type="text" maxlength="30" name="coordY" required placeholder="1.0" value="<%=store.getCoordY()%>"/>
      <br />
      Categoria: 
       <select name="type" required>
      		<option value=""></option>
      		<%
      		if (listOfCategorys != null){
      			Category categoryStore = store.getCategory();
      			for (Category category : listOfCategorys){
      		%>
      			<option value="<%=category.getId()%>" <%=category.getId()==categoryStore.getId()?"selected":""%>><%=category.getName()%></option> 
      		<%
      			}
      		}
      		%>
      </select>
      <br />
       Nivel do preço: 
      <select name="preco" required>
      		<option value="1" <%=store.getPriceLvl()==1?"selected":""%>>$</option>
      		<option value="2" <%=store.getPriceLvl()==2?"selected":""%>>$$</option>
      		<option value="3" <%=store.getPriceLvl()==3?"selected":""%>>$$$</option>
      		<option value="4" <%=store.getPriceLvl()==4?"selected":""%>>$$$$</option>
      		<option value="5" <%=store.getPriceLvl()==5?"selected":""%>>$$$$$</option>
      </select>
      <br />
      <input type="submit" value="Submit" onclick="submitForm();" />
    </form>
    
    <%}else{ %>
    	<h2>Erro ao obter a loja</h2>
    <%} %>
   </body>
</html>