<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<%@page import="br.com.beauty.manager.PropertiesController"%>
<html>
  <head>
  	<%@ include file="../headImports.html"%>
    <title>Beauty Pass - Admin</title>
    <meta charset="utf-8"/>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  </head>
  <body>
		<a href="/documentacao/documentacao.jsp">voltar</a>
		<h1>BEAUTY PASS - WEBSERVICE</h1>
		
		<br><h2>CHAMADAS REST</h2>
		
		<p>CONSULTA DE LOJAS</p>
		<p>Tipo: GET</p>
		<p>URL: <u>/stores</u></p>
		<p>DESCRIÇÃO: Obtem a lista de lojas de acordo com os parâmetros</p>
		<p>Parâmetros:</p>
		<table style="width:100%" border="1">
			  <tr>
			    <th>Key</th>
			    <th>Tipo de dado</th>
			    <th>Obrigatorio</th>
			    <th style="width:40%">Descrição</th>
			  </tr>
			  <tr>
			    <th>coordenada-x</th>
			    <th>Double</th>
			    <th>False</th>
			    <th>Coordenada com a latitude referente ao ponto inicial da busca, este campo ultiliza a propriedade de ambiente "raioDasLojas" para calcular as lojas proximas em determinado raio</th>
			  </tr>
			  <tr>
			    <th>coordenada-y</th>
			    <th>Double</th>
			    <th>False</th>
			    <th>Coordenada com a longitude referente ao ponto inicial da busca, este campo ultiliza a propriedade de ambiente "raioDasLojas" para calcular as lojas proximas em determinado raio</th>
			  </tr>
		</table>
		<p>Retorno: <a href="examples/stores.json" target="_blank">stores.json</a></p>
		
  </body>
</html>
