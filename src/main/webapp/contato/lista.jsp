<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<%@page import="br.com.beauty.model.*,
		br.com.beauty.manager.*,
		java.util.ArrayList"%>
  <head>
  	<%@ include file="../headImports.html"%>
    <title>Contato</title>
    <meta charset="utf-8"/>
  </head>
  <body>
  	<%
  		
  		
  	%>
  <a href="/">voltar</a>
    <h1>Lista de contatos</h1>
    <br/>
    <% 
    	ArrayList<Contact> listOfContacts = ContactController.getInstance().listOfContacts();	
    	
    	if (listOfContacts != null && !listOfContacts.isEmpty()){
    %></br>
      <table id="tableServices" style="width:100%" border="1">
		  <tr>
		  	<th>id</th>
		  	<th>Id serviço</th>
		  	<th>Id user</th>
		  	<th>Id user</th>
		  	<th>Data</th>
		  	<th>Detalhe</th>
		  </tr>
		  <%
		  for (Contact contact : listOfContacts){
		  %>
		   <tr>
		   	   <th><%=contact.getId()%></th>
		   	   <th><a href="../servicoAgendado.jsp?id=<%=contact.getIdServiceExecuted()%>"><%=contact.getIdServiceExecuted()%></a></th>
		   	   <th><%=contact.getUser().getId()%></th>
		   	   <th><%=contact.getUser().getNome()%></th>
		   	   <th><%=contact.getDateSend()%></th>
		   	   <th><button class="details">Detalhes</button></th>
		   	   <th style="display:none;"><textarea><%=contact.getMessage()%></textarea></th>
		 	</tr>
		  <%
		  }
		  %>
		</table>
		<script>
			$("button").click(function() {
				newpopupWindow = window.open ('', 'pagina', "width=250 height=250");
				newpopupWindow.document.write ($(this).parent().parent().find('textarea').text());	
	    	});
		</script>
		
	    <%}else if (listOfContacts != null){%>
	    	<h2>Lista vazia</h2>
	    <%}else{%>
	    	<h2>Erro ao obter os dados</h2>
	    <%}%>
    	
   </body>
</html>