<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<%@page import="br.com.beauty.model.Category,
		br.com.beauty.manager.StoreController,
		java.util.ArrayList"%>
  <head>
  	<%@ include file="headImports.html"%>
    <title>Cadastro de nova loja</title>
    <meta charset="utf-8"/>
  </head>
  <body>
  	<%
  		ArrayList<Category> listOfCategorys = StoreController.getInstance().getListOfCategorys();
  	%>
  <a href="/listaDeLojas.jsp">voltar</a>
    <h1>Cadastro de nova loja</h1>
    <br/>
    <form action="addStore" method="POST">
      Título da loja: <input type="text" name="title" maxlength="255">
      <br />
      Introdução: <input type="text" name="introduction" required/>
      <br />
      Descrição: <textarea type="text" name="description"></textarea>
      <br />
      email: <input type="email" name="email" required/>
      <br />
      telefone: <input type="text" name="tel" required/>
      <br />
      senha: <input type="password" name="password" required/>
      <br />
      cnpj: <input type="number" name="cnpj" required />
      <br />
      endereco: <input type="text" name="endereco" required/>
      <br />
      CEP: <input type="number" maxlength="9" name="cep" required/>
      <br />
      Coord X: <input type="text" maxlength="30" name="coordX" required placeholder="1.0"/>
      <br />
      Coord Y: <input type="text" maxlength="30" name="coordY" required placeholder="1.0"/>
      <br />
      Categoria: 
      <select name="type" required>
      		<option value=""></option>
      		<%
      		if (listOfCategorys != null){
      			for (Category category : listOfCategorys){
      		%>
      			<option value="<%=category.getId()%>"><%=category.getName()%></option> 
      		<%
      			}
      		}
      		%>
      </select>
      <br />
       Nivel do preço: 
      <select name="preco" required>
      		<option value="1">$</option>
      		<option value="2">$$</option>
      		<option value="3">$$$</option>
      		<option value="4">$$$$</option>
      		<option value="5">$$$$$</option>
      </select>
      <br />
      <input type="submit" value="Submit" />
    </form>
   </body>
</html>