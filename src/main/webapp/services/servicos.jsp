<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<%@page import="br.com.beauty.model.*,
		br.com.beauty.manager.*,
		java.util.ArrayList"%>
  <head>
  	<%@ include file="../headImports.html"%>
    <title>Editar loja</title>
    <meta charset="utf-8"/>
  </head>
  <body>
  	<%
  		String idOnParameter = request.getParameter("storeId");
	  	Store store = null;	
	  	
	  	if (idOnParameter != null){
	  		
	  		int id;
	  		try{
	  			id = Integer.parseInt(idOnParameter);
	  			store = StoreController.getInstance().getStore(id);	
	  		}catch(NumberFormatException e){
	  			System.out.print("Erro ao obter os dados do id para editar a loja");
	  		}
	  	}
  		
  	%>
  <a href="/listaDeLojas.jsp">voltar</a>
    <h1>Editar Serviços de loja</h1>
    <br/>
    <% 
    if (store != null){
    	
    	ArrayList<Service> listOfServices = ServiceController.getInstance().getServices(store.getId());	
    %>
    <form action="/serviceResource" method="POST">
   	  <input type="hidden" name="id" value="<%=store.getId()%>">
       ID: <input type="number" disabled value="<%=store.getId()%>">
    	<br />
      Título da loja: <input type="text" name="title" disabled maxlength="255" value="<%=store.getTitle()%>">
      <br />
      Lista de serviços:
      <input id="servicesToEdit" name="servicesToEdit" type="hidden" value="<%=store.getJSONServices()%>"/>
      <input id="servicesIdToDelete" name="servicesToDelete" type="hidden" value=""/>
      <br />
      <button id="add" onclick="window.open('/services/AddServico.jsp?idStore=<%=store.getId()%>', '_blank'); return false;">Adicionar novo serviço</button>
      <button id="revert" onclick="return false;">Reverter linha deletada</button>
      <button id="segments" onclick="window.open('/services/Segments.jsp?idStore=<%=store.getId()%>'); return false;">Lista de segmentos</button>
      </br>
      <table id="tableServices" style="width:100%" border="1">
		  <tr>
		  	<th style="display:none">id</th>
		  	<th>Título</th>
		  	<th>Ativo</th>
		  	<th>Destaque</th>
		  	<th>Posição</th>
		  	<th>Segmento</th>
		  	<th>Editar</th>
		  	<th>Remover</th>
		  </tr>
		  <%
		  for (Service service : listOfServices){
		  %>
		   <tr>
		   	   <th style="display:none"><input type="text" value="<%=service.getId()%>" required/></th>
		 	   <th><%=service.getTitle()%></th>
			   <th><input type="checkbox" <%=service.isValid()?"checked":""%>/></th>
			   <th><input type="checkbox" <%=service.isHightlight()?"checked":""%>/></th>
			   <th><input type="number" value="<%=service.getPosition()%>" required/></th>
			   <th><%=service.getSegment()==null?"":service.getSegment().getName()%></th>
			   <th><button onclick="window.open('/services/EditServico.jsp?id=<%=service.getId()%>', '_blank'); return false;" class="remove_button">Editar</button></th>
			   <th><button onclick="removeRow(this);" class="remove_button">Remover</button></th>
		 	</tr>
		  <%
		  }
		  %>
	</table>
      <input type="submit" value="Submit" onclick="submitForm();" />
    </form>
    <script>
    
    	var rowsRemoved = [];
    	var servicesIdsToDelete = [];
    	
    	//Remove a linha 
    	function removeRow(button){
    		var rowToRemove = $(button).parent().parent();
    		var idRow = $($(rowToRemove).find("th")[0]).find('input').val();
    		
    		if (idRow != -1)
    			servicesIdsToDelete.push(parseInt(idRow));
    		
    		rowsRemoved.push(rowToRemove);
    		rowToRemove.remove();
    		
    		refreshValuesServices();
    		return false;
    	}
    	
    	//Reverte a linha deletada
		$("#revert").click(function() {
    		var row = rowsRemoved.pop();
    		if (row){
    			var id = $(row).find("input").val();
    			servicesIdsToDelete.pop(parseInt(id));
    			$("#tableServices").find("tbody").append(row);
    		}
    		refreshValuesServices();
    	});
    	
    	//Atualiza os valores pro JSON
    	function refreshValuesServices(){
    		var rows = $("#tableServices").find("tbody").find("tr");
    		
    		var jsonServicesAdd = [];
    		var jsonServicesEdit = {};
    		
    		for (var row in rows){
    			
    			if (row > 0){
	    			var objRow = getObjectRow(rows[row]);
	    			
					if (objRow != null){
		    			jsonServicesEdit[parseInt(objRow.id)] = objRow;
    				}
    			}
    		}
    		
   			$("#servicesToEdit").val(JSON.stringify(jsonServicesEdit));
    		$("#servicesIdToDelete").val(JSON.stringify(servicesIdsToDelete));
    	}
    	
    	//Obtem o objeto de acordo com as linhas
    	function getObjectRow(row){
    		
    		var obj = {};
    		var colunas = $(row).find("th");
    		
    		var id = $(colunas[0]).find("input").val();
			var valid = $(colunas[2]).find("input").prop("checked");
			var highlight = $(colunas[3]).find("input").prop("checked");
			var position = $(colunas[4]).find("input").val();
		
			if (position){
				obj.id = id;
				obj.valid = valid;
				obj.highlight = highlight;
				obj.position = parseInt(position);
    			return obj;
			}else{
				return null;
			}
    	}
    	
    	function submitForm(){
    		refreshValuesServices();
    		return false;
    	}
    	
    </script>
    <%}else{ %>
    	<h2>Erro ao obter a loja</h2>
    <%} %>
   </body>
</html>