<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<%@page import="br.com.beauty.model.Category,
		br.com.beauty.manager.*,
		java.util.ArrayList"%>
  <head>
  	<%@ include file="../headImports.html"%>
    <title>Segmentos</title>
    <meta charset="utf-8"/>
  </head>
  <body>
  	<%
  	  String storeId = request.getParameter("idStore");
  	
	   int idStore = Integer.parseInt(storeId);
	   ArrayList<Category> listOfTypesServices = ServiceController.getInstance().getSegmentsStore(idStore);
	  	
  	%>
  <a href="/services/servicos.jsp?storeId=<%=storeId%>">voltar</a>
    <h1>Segmentos</h1>
    <br/>
    <form action="/segmentManager" method="POST" onsubmit="return submitForm()">
      <input type="text" name="storeId" value="<%=storeId%>" style="display:none;"/>
      Lista de segmentos:
      <input id="segmentsAdd" name="segmentsToAdd" type="hidden" />
      <input id="segmentsEdit" name="segmentsToEdit" type="hidden" />
      <input id="segmentsDelete" name="segmentsToDelete" type="hidden" />
      <br />
      <button id="add" onclick="return false;">Adicionar uma novo segmento</button>
     </br>
      <table id="tableSegments" style="width:50%" border="1">
		  <tr>
		  	<th style="display:none">id</th>
		  	<th>Título</th>
		  	<th>Remover</th>
		  </tr>
		  <%
		  for (Category segment : listOfTypesServices){
		  %>
		   <tr>
		   	   <th style="display:none"><input type="text" value="<%=segment.getId()%>" required/></th>
		 	   <th><input type="text" value="<%=segment.getName()%>" required/></th>
			   <th><button onclick="removeRow(this);" class="remove_button">Remover</button></th>
		 	</tr>
		  <%
		  }
		  %>
		</table>
      <input type="submit"  value="Submit" />
    </form>
    <script>
    
    	var jsonServicesAdd = [];
    	var rowsAdded = [];
    	var rowsRemoved = [];
    	var idsToDelete = [];
    	
    	$("#add").click(function() {
    		var htmlRow = '<tr>';
    		htmlRow += '<th style="display:none"><input type="number" required value="-1"/></th>'
    		htmlRow += '<th><input type="text"/></th>'
			htmlRow += '<th><button onclick="removeRow(this);" class="remove_button">Remover</button></th>'
				    		
    		$("#tableSegments").find("tbody").append(htmlRow+"</tr>");
    	});
    	
    	//Remove a linha 
    	function removeRow(button){
    		var rowToRemove = $(button).parent().parent();
    		var idRow = $($(rowToRemove).find("th")[0]).find('input').val();
    		
    		if (idRow != -1)
    			idsToDelete.push(parseInt(idRow));
    		
    		rowsRemoved.push(rowToRemove);
    		rowToRemove.remove();
    		
    		refreshValuesServices();
    		return false;
    	}
    	
    	//Reverte a linha deletada
		$("#revert").click(function() {
    		var row = rowsRemoved.pop();
    		if (row){
    			var id = $(row).find("input").val();
    			idsToDelete.pop(parseInt(id));
    			$("#tableServices").find("tbody").append(row);
    		}
    		refreshValuesServices();
    	});
    	
    	//Atualiza os valores pro JSON
    	function refreshValuesServices(){
    		var rows = $("#tableSegments").find("tbody").find("tr");
    		
    		var jsonServicesEdit = {};
    		
			for (var row in rows){
    			
    			if (row > 0){
	    			var objRow = getObjectRow(rows[row]);
	    			
					if (objRow != null){
		    			if (objRow.id == -1){
		    				rowsAdded.push(objRow.name);
		    			}else{
		    				jsonServicesEdit[parseInt(objRow.id)] = objRow.name;
		    			}
    				}
    			}
    		}
    		
   			$("#segmentsAdd").val(JSON.stringify(rowsAdded));
   			$("#segmentsEdit").val(JSON.stringify(jsonServicesEdit));
    		$("#segmentsDelete").val(JSON.stringify(idsToDelete));
    	}
    	
    	//Obtem o objeto de acordo com as linhas
    	function getObjectRow(row){
    		
    		var obj = {};
    		var colunas = $(row).find("th");

			var id = $(colunas[0]).find("input").val();
			var name = $(colunas[1]).find("input").val();
		
			if (name && id ){
				obj.name = name;
				obj.id = id;
    			return obj;
			}else{
				return null;
			}
    	}
    	
    	function submitForm(){
    		refreshValuesServices();
    		return true;
    	}
    </script>
   </body>
</html>