create database beautyDB
use beautyDB

create table tipoCliente(
    id int NOT NULL,
    nome varchar(32) NOT NULL,
    PRIMARY KEY (id)
)

insert into tipoCliente (id, nome) value (0, 'guest')

create table cliente(
    id int NOT NULL AUTO_INCREMENT,
    nome varchar(255) NOT NULL,
    cpf varchar(255) NOT NULL,
    tipo int NOT NULL default 0,
    valido boolean default true,
    criacao datetime NOT NULL,
    ipCriacao varchar(32),
    localCriacao varchar(32),
    PRIMARY KEY (id),
    FOREIGN KEY (tipo) REFERENCES tipoCliente(id)
)

ALTER TABLE cliente AUTO_INCREMENT=100;
ALTER TABLE cliente ADD COLUMN localCriacao varchar(64);
ALTER TABLE cliente ADD COLUMN criacao DATETIME;
ALTER TABLE cliente DROP COLUMN criacao;
UPDATE cliente set criacao = '2020-06-02 00:29:18.0'

drop table cliente

select * from cliente

drop table estabelecimento

select * from estabelecimento

create table estabelecimento(
    id int NOT NULL AUTO_INCREMENT,
    nome varchar(255) NOT NULL,
    introducao varchar(255),
    descricao longtext,
    email varchar(64) NOT NULL,
    telefone varchar(32),
    senha varchar(32) NOT NULL,
    cnpj varchar(255) NOT NULL,
    endereco varchar(255) NOT NULL,
    cep varchar(16) NOT NULL,
    complemento varchar(255),
    valido boolean default true,
    coordX float default true,
    coordY float default true,
    dataCriacao date not null,
    PRIMARY KEY (id)
)

create table properties(
    chave varchar(32) not null,
    valor varchar(255) not null,
    protecao int default 0,
    PRIMARY KEY (chave)
)limiteTiposLojaDestaque

insert into properties (chave, valor) value ('raioDasLojas', '50') 
insert into properties (chave, valor) value ('limiteDestaque', '3') 
insert into properties (chave, valor) value ('limiteTiposLojaDestaque', '-1') 
insert into properties (chave, valor) value ('qtImages', '7') 

create table categoria_estabelecimento(
    id int NOT NULL AUTO_INCREMENT,
    nome_categoria varchar(128) NOT NULL,
    PRIMARY KEY (id)
)

ALTER TABLE categoria_estabelecimento ADD COLUMN imageId INT;
update categoria_estabelecimento set imageId = id where id = 5
insert into categoria_estabelecimento (nome_categoria) values ('Sal�o / Barbearia'),('Barbearia'),('Sal�o'),('Spa'),('Sal�o / Spa')

ALTER TABLE estabelecimento ADD COLUMN categoria INT DEFAULT 1;
ALTER TABLE estabelecimento ADD COLUMN nota FLOAT DEFAULT 5;
ALTER TABLE estabelecimento ADD COLUMN precoLvl int DEFAULT 3;
ALTER TABLE estabelecimento ADD COLUMN imageid int DEFAULT 0;
ALTER TABLE estabelecimento add FOREIGN KEY (categoria) REFERENCES categoria_estabelecimento(id) ON DELETE CASCADE;


update estabelecimento set imageid = 1 where id = 3
update estabelecimento set descricao = 'DESCRI��O DA LOJA XYZ'
update estabelecimento set categoria = 3 where id > 4

SELECT est.*, cat.nome_categoria AS nomeCategoria  FROM estabelecimento as est INNER JOIN categoria_estabelecimento as cat
ON est.categoria = cat.id  ORDER BY est.dataCriacao

create table servico_loja(
    id int NOT NULL AUTO_INCREMENT,
    titulo varchar(255) NOT NULL,
    descricao longtext,
    valor double NOT NULL,
    id_loja int NOT NULL,
    ordem int NOT NULL default 0,
    ativo boolean default true,
    PRIMARY KEY (id),
    FOREIGN KEY (id_loja) REFERENCES estabelecimento(id)
)

SELECT * FROM estabelecimento INNER JOIN categoria_estabelecimento as cat ON estabelecimento.categoria = cat.id WHERE estabelecimento.id = 4 ORDER BY dataCriacao

drop table opcao_servico

create table opcao_servico(
    id int NOT NULL AUTO_INCREMENT,
    id_servico int NOT NULL,
    titulo varchar(255) NOT NULL,
    descricao longtext,
    valor double NOT NULL,
    ordem int NOT NULL default 0,
    valido boolean default true,
    PRIMARY KEY (id),
    FOREIGN KEY (id_servico) REFERENCES servico_loja(id)
)

insert into tipo_servico (title) values ('SPAs'),('Corte de Cabelo'),('Limpeza de pele')

select * from tipo_servico

select * from opcao_servico where id_servico in (select id from servico_loja where id_loja = 1)

create table segmento_servico(
    id int NOT NULL AUTO_INCREMENT,
    title varchar(255) NOT NULL,
    id_loja int NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (id_loja) REFERENCES estabelecimento(id)
)

ALTER TABLE servico_loja ADD COLUMN segmento INT;
ALTER TABLE servico_loja add FOREIGN KEY (segmento) REFERENCES segmento_servico(id);
ALTER TABLE servico_loja ADD COLUMN diasSemana varchar(255);
ALTER TABLE servico_loja ADD COLUMN horaInicio TIME;
ALTER TABLE servico_loja ADD COLUMN horaFim TIME;
ALTER TABLE servico_loja ADD COLUMN duration TIME;
ALTER TABLE servico_loja ADD COLUMN createDate DATE;
ALTER TABLE servico_loja ADD COLUMN limite int default 3;

insert into properties (chave, valor) value ('limitDiasCadatro', '15') 

create table servico_agendado(
    id int NOT NULL AUTO_INCREMENT,
    dateTime long NOT NULL,
    dataAgendada date not null,
    horaAgendada time not null,
    idUser int NOT NULL,
    idLoja int NOT NULL,
    idServico int,
    nomeServico varchar(255),
    total double NOT NULL,
    avaliacao int,
    confirmado boolean default false,
    opcoes LONGTEXT not null,
    dataAgendamento DATETIME NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (idUser) REFERENCES cliente(id),
    FOREIGN KEY (idLoja) REFERENCES estabelecimento(id)
)

ALTER TABLE servico_agendado AUTO_INCREMENT=5000;
drop table servico_agendado 

SELECT servico_loja.titulo as 'nameService', estabelecimento.nome as 'titleStore', estabelecimento.id as 'idStore'
				FROM servico_loja INNER JOIN
				estabelecimento ON servico_loja.id_loja = estabelecimento.id 
				WHERE servico_loja.id = 28
                
SELECT now()

update servico_agendado set confirmado = ? where id = ?

CREATE TABLE contato(
    id int NOT NULL AUTO_INCREMENT,
    idServicoAgendado int NOT NULL,
    texto LONGTEXT NOT NULL,
    dataEnvio DATETIME NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (idServicoAgendado) REFERENCES servico_agendado(id)
)

CREATE TRIGGER atualizar_nota AFTER UPDATE
ON servico_agendado
FOR EACH ROW
BEGIN
    UPDATE estabelecimento SET nota = (SELECT AVG(avaliacao) FROM servico_agendado 
    WHERE avaliacao > -1 AND idLoja = OLD.idLoja) WHERE ID = OLD.idLoja;
END

SELECT id,nota FROM estabelecimento

SELECT AVG(avaliacao) FROM servico_agendado 
    WHERE avaliacao > -1 AND idLoja = OLD.idLoja
    
UPDATE servico_agendado SET avaliacao = -1 WHERE idLoja = 3

SELECT estabelecimento.id, quantidade_avaliacoes.avaliacoes
as qt from estabelecimento inner join (SELECT idLoja, COUNT(avaliacao) AS "avaliacoes" FROM servico_agendado WHERE avaliacao > -1 GROUP BY idLoja) 
AS quantidade_avaliacoes on quantidade_avaliacoes.idLoja = estabelecimento.id 
WHERE servico_agendado.avaliacao > -1


SELECT estabelecimento.*, cat.nome_categoria,  FROM estabelecimento
 INNER JOIN categoria_estabelecimento as cat ON estabelecimento.categoria = cat.id 
 INNER JOING (SELECT idLoja, COUNT(avaliacao) AS "avaliacoes" FROM servico_agendado WHERE avaliacao > -1 GROUP BY idLoja) 
    AS quantidade_avaliacoes on quantidade_avaliacoes.idLoja = estabelecimento.id 



CREATE TABLE LOGGER(
   id int NOT NULL AUTO_INCREMENT,
   title varchar(255) NOT NULL,
   ip varchar(32),
    dataEnvio DATETIME NOT NULL,
    PRIMARY KEY (id)
)

delete from LOGGER

SELECT * FROM servico_loja where titulo LIKE '%servi�o%7%'

SELECT * FROM servico_loja WHERE titulo REGEXP 'servi�o|p�'

SELECT servico_loja.id, agenda.avaliacao, agendaQt.qt, valueService.valueFinal FROM servico_loja 
left join (select idServico, AVG(avaliacao) as avaliacao from servico_agendado WHERE avaliacao > 0 GROUP BY idServico) AS agenda ON servico_loja.id = agenda.idServico 
left join (select idServico, COUNT(id) as qt from servico_agendado GROUP BY idServico) AS agendaQt ON servico_loja.id = agendaQt.idServico 
left join (select idServico, SUM(total) as valueFinal from servico_agendado WHERE confirmado < 1 GROUP BY idServico) AS valueService ON servico_loja.id = valueService.idServico


SELECT estabelecimento.nome, estabelecimento.nota, (SELECT COUNT(ID) FROM servico_loja WHERE id_loja = 8) AS quant,
        (SELECT COUNT(ID) FROM servico_agendado WHERE confirmado = -1 and idLoja = 8) AS notConfirmed,
        (SELECT COUNT(ID) FROM servico_agendado WHERE confirmado = 0 and idLoja = 8) AS confirmed,
        (SELECT COUNT(ID) FROM servico_agendado WHERE confirmado = 1 and idLoja = 8) AS declined,
        (SELECT SUM(total) FROM servico_agendado WHERE  idLoja = 8) AS value
        from estabelecimento WHERE estabelecimento.id = 8 
        
        
    